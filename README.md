# Project "Soc *** Net"  

Maven multi-module project that simulates social network.

Demo may be observer on Heroku by link: [Soc *** Net](https://socialnetworkapppet.herokuapp.com/).

Admin account: admin@gmail.com  
Password: 5432 

** Functionality for all users: **

* Registration  
* Login/Logout
* View own profile
* View other users profiles
* Change own user profile
* Send message through WebSocket
* AJAX users/groups search
* Users/group pagination
* User profile export to XML / import from XML
* Posting message on wall
* Add and remove friends 
* Friends requests and friendship acceptance
* Make and join Groups

** Functionality for admin: **

* Change all user profiles(except passwords)
* Delete user profile
* Make other user admin

** Tools: **
JDK 8, Spring 5, JPA 2 / Hibernate 5, jQuery 3, Bootstrap 4, JUnit 5, Mockito, Maven 3, Git / Bitbucket, Tomcat 9, MySQL, JavaScript, JQuery, H2/MySql,  IntelliJIDEA 2020.  

** Screenshots: **

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/account.jpg) 

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/password.jpg)  

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/group.jpg)

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/sms.jpg)  

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/friends.jpg)  

![picture](https://bitbucket.org/dserebryakova/social-network-2019-serebryakovad/raw/master/screenshot/admin.jpg) 
  
 