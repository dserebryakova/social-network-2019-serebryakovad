package com.getjavajob.dao.dao.impl;

import com.getjavajob.common.Account;
import com.getjavajob.common.Group;
import com.getjavajob.common.GroupMember;
import com.getjavajob.common.enums.GroupRole;
import com.getjavajob.dao.dao.GroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.getjavajob.common.enums.GroupRole.*;

@Repository
@Transactional
public class GroupDaoImpl implements GroupDao<Group, Long> {
    @PersistenceContext
    private EntityManager entityManager;

    private final AccountDaoImpl accountDaoImpl;

    @Autowired
    public GroupDaoImpl(AccountDaoImpl accountDaoImpl) {
        this.accountDaoImpl = accountDaoImpl;
    }

    public void create(Group group) {
        entityManager.persist(group);
    }

    public void insertMember(Long idGroup, Long idMember) {
        Account member = accountDaoImpl.getId(idMember);
        Group group = getId(idGroup);
        GroupMember groupMember = new GroupMember();
        groupMember.setGroup(group);
        groupMember.setMember(member);
        groupMember.setGroupRole(SUBSCRIBER);
        entityManager.persist(groupMember);
    }

    public List<Group> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> root = query.from(Group.class);
        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    public Group getId(Long id) {
        return entityManager.find(Group.class, id);
    }

    public List<GroupMember> getAllMemberFromGrMem(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> groupRoot = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> memberSelect = criteriaQuery.select(groupRoot).where(
                criteriaBuilder.equal(groupRoot.get("group"), getId(id)));
        return entityManager.createQuery(memberSelect).getResultList();
    }

    public List<Group> getByIdOwner(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> groupRoot = criteriaQuery.from(Group.class);
        CriteriaQuery<Group> ownerSelect = criteriaQuery.select(groupRoot).where(
                criteriaBuilder.equal(groupRoot.get("idOwner"), id));
        return entityManager.createQuery(ownerSelect).getResultList();
    }

    public List<Group> getMembership(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> groupRoot = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> ownerSelect = criteriaQuery.select(groupRoot).where(
                criteriaBuilder.equal(groupRoot.get("member"), id));
        List<GroupMember> membership = entityManager.createQuery(ownerSelect).getResultList();
        Set<Group> result = new HashSet<>();
        for (GroupMember member : membership) {
            result.add(member.getGroup());
        }
        return new ArrayList<>(result);
    }

    public List<GroupMember> getMembers(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> groupRoot = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> memberSelect = criteriaQuery.select(groupRoot).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(groupRoot.get("group"), getId(id)),
                        criteriaBuilder.notEqual(groupRoot.get("groupRole"), SUBSCRIBER)
                ));
        return entityManager.createQuery(memberSelect).getResultList();
    }

    public List<GroupMember> getSubscribes(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> groupRoot = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> memberSelect = criteriaQuery.select(groupRoot).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(groupRoot.get("group"), getId(id)),
                        criteriaBuilder.equal(groupRoot.get("groupRole"), SUBSCRIBER)
                ));
        return entityManager.createQuery(memberSelect).getResultList();
    }

    public void update(Group group) {
        entityManager.merge(group);
    }

    public void updateModer(Long id) {
        GroupMember groupMember = getGroupMemberById(id);
        groupMember.setGroupRole(MODER);
        entityManager.merge(groupMember);
    }

    public GroupMember getGroupMemberById(Long id) {
        return entityManager.find(GroupMember.class, id);
    }

    public void updateMember(Long id) {
        GroupMember groupMember = getGroupMemberById(id);
        groupMember.setGroupRole(MEMBER);
        entityManager.merge(groupMember);
    }

    public void delete(Long id) {
        Group group = getId(id);
        entityManager.remove(group);
    }

    public void deleteMember(Long idGroup, Long idMember) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
        Root<GroupMember> groupMemberRoot = criteriaQuery.from(GroupMember.class);
        CriteriaQuery<GroupMember> firstSelect = criteriaQuery.select(groupMemberRoot).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(groupMemberRoot.get("group"), getId(idGroup)),
                        criteriaBuilder.equal(groupMemberRoot.get("member"), accountDaoImpl.getId(idMember))));
        GroupMember deleteGroupMember = entityManager.createQuery(firstSelect).getSingleResult();
        entityManager.remove(deleteGroupMember);
    }

    public GroupRole getRole(Long idGroup, Long idMember) {
        GroupMember groupMember;
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
            Root<GroupMember> groupMemberRoot = criteriaQuery.from(GroupMember.class);
            CriteriaQuery<GroupMember> firstSelect = criteriaQuery.select(groupMemberRoot).where(
                    criteriaBuilder.and(
                            criteriaBuilder.equal(groupMemberRoot.get("group"), getId(idGroup)),
                            criteriaBuilder.equal(groupMemberRoot.get("member"), accountDaoImpl.getId(idMember))));
            groupMember = entityManager.createQuery(firstSelect).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
        return groupMember.getGroupRole();
    }

    public List<Group> searchGroup(String search) {
        return entityManager.createQuery(getCriteriaQuerySearchGroup(search)).getResultList();
    }

    private CriteriaQuery<Group> getCriteriaQuerySearchGroup(String search) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = builder.createQuery(Group.class);
        Root<Group> root = criteriaQuery.from(Group.class);
        return criteriaQuery.select(root).where(builder.or(builder.like(builder.lower(root.get("name")), "%" + search + "%")));
    }

    public List<Group> getPageGroup(String search, int startPage, int maxPage) {
        return entityManager.createQuery(getCriteriaQuerySearchGroup(search)).setFirstResult(startPage).setMaxResults(maxPage).getResultList();
    }

    public int countSearchResultGroup(String search) {
        return entityManager.createQuery(getCriteriaQuerySearchGroup(search)).getResultList().size();
    }

    public byte[] getPhoto(Long id) {
        Group group = getId(id);
        return group.getPhoto();
    }

    public Account getInfoOwner(Long idGroup, Long idOwner) {
        Group group;
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
            Root<Group> groupMemberRoot = criteriaQuery.from(Group.class);
            CriteriaQuery<Group> firstSelect = criteriaQuery.select(groupMemberRoot).where(
                    criteriaBuilder.and(
                            criteriaBuilder.equal(groupMemberRoot.get("id"), idGroup),
                            criteriaBuilder.equal(groupMemberRoot.get("idOwner"), idOwner)));
            group = entityManager.createQuery(firstSelect).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
        return accountDaoImpl.getId(group.getIdOwner());
    }

    public boolean checkUsersHaveMemberShip(Long idGroup, Long idMember) {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<GroupMember> criteriaQuery = criteriaBuilder.createQuery(GroupMember.class);
            Root<GroupMember> groupMemberRoot = criteriaQuery.from(GroupMember.class);
            CriteriaQuery<GroupMember> firstSelect = criteriaQuery.select(groupMemberRoot).where(
                    criteriaBuilder.and(
                            criteriaBuilder.equal(groupMemberRoot.get("group"), idGroup),
                            criteriaBuilder.equal(groupMemberRoot.get("member"), idMember)
                    ));

            entityManager.createQuery(firstSelect).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }
}
