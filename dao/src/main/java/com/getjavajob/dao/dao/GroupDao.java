package com.getjavajob.dao.dao;

import com.getjavajob.common.Account;
import com.getjavajob.common.GroupMember;
import com.getjavajob.common.enums.GroupRole;

import java.util.List;

public interface GroupDao<T, ID> {
    void create(T t);

    T getId(ID id);

    void update(T t);

    void delete(ID id);

    void deleteMember(ID idGroup, ID idMember);

    void insertMember(ID idGroup, ID idMember);

    void updateModer(ID id);

    void updateMember(ID id);

    List<T> getAll();

    List<T> getByIdOwner(ID id);

    List<T> getMembership(ID id);

    byte[] getPhoto(ID id);

    List<GroupMember> getAllMemberFromGrMem(ID id);

    List<GroupMember> getMembers(ID id);

    List<GroupMember> getSubscribes(ID id);

    GroupMember getGroupMemberById(ID id);

    GroupRole getRole(ID idGroup, ID idMember);

    List<T> searchGroup(String search);

    List<T> getPageGroup(String search, int startPage, int maxPage);

    int countSearchResultGroup(String search);

    Account getInfoOwner(ID idGroup, ID idOwner);

    boolean checkUsersHaveMemberShip(ID idGroup, ID idOwner);
}
