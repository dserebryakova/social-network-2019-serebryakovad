package com.getjavajob.dao.dao;

import java.util.List;

public interface AccountDao<T, ID> {
    void create(T t);

    T getId(ID id);

    void update(T t);

    void delete(ID id);

    List<T> getAll();

    byte[] getPhoto(ID id);

    T getAccountByEmail(String email);

    List<T> searchAccounts(String search);

    List<T> getPageAccounts(String search, int startPage, int maxPage);

    int countSearchResultAccount(String search);
}
