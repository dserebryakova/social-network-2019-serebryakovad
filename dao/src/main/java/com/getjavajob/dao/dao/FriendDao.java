package com.getjavajob.dao.dao;

import com.getjavajob.common.Account;

import java.util.List;

public interface FriendDao<T, ID> {
    void create(T t);

    T getId(ID id);

    void requestFriend(T t);

    List<Account> getAllFriendById(ID id);

    List<Account> getSubscriptions(ID id);

    List<Account> getAllReqFriendById(ID id);

    void delete(T t);

    boolean checkUsersHaveFriendShip(ID user, ID friend, boolean accepted);
}
