package com.getjavajob.dao.dao.impl;

import com.getjavajob.common.Phone;
import com.getjavajob.dao.dao.PhoneDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class PhoneDaoImpl implements PhoneDao<Phone, Long> {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Phone> getPhonesByOwnerId(Long ownerId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Phone> criteriaQuery = criteriaBuilder.createQuery(Phone.class);
        Root<Phone> phoneRoot = criteriaQuery.from(Phone.class);
        CriteriaQuery<Phone> firstSelect = criteriaQuery.select(phoneRoot).where(
                criteriaBuilder.equal(phoneRoot.get("ownerPhone"), ownerId));
        return entityManager.createQuery(firstSelect).getResultList();
    }

    public void create(Phone phone) {
        entityManager.persist(phone);
    }

    public Phone getId(Long id) {
        return entityManager.find(Phone.class, id);
    }

    public void delete(Long id) {
        Phone phone = getId(id);
        entityManager.remove(phone);
    }
}
