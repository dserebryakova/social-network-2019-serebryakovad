package com.getjavajob.dao.dao.impl;

import com.getjavajob.common.Account;
import com.getjavajob.dao.dao.AccountDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
public class AccountDaoImpl implements AccountDao<Account, Long> {
    @PersistenceContext
    private EntityManager entityManager;

    public void create(Account account) {
        entityManager.merge(account);
    }

    public List<Account> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> root = query.from(Account.class);
        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    public Account getId(Long id) {
        return entityManager.find(Account.class, id);
    }

    public void update(Account account) {
        entityManager.merge(account);
    }

    public void delete(Long id) {
        Account account = getId(id);
        entityManager.remove(account);
    }

    public byte[] getPhoto(Long id) {
        return getId(id).getPhoto();
    }

    public Account getAccountByEmail(String email) {
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> friendRoot = criteriaQuery.from(Account.class);
            CriteriaQuery<Account> firstSelect = criteriaQuery.select(friendRoot).where(
                    criteriaBuilder.and(
                            criteriaBuilder.equal(friendRoot.get("email"), email)));
            return entityManager.createQuery(firstSelect).getSingleResult();
        } catch (NoResultException ignored) {
        }
        return null;
    }

    public List<Account> searchAccounts(String search) {
        return entityManager.createQuery(getCriteriaQuerySearchAccount(search)).getResultList();
    }

    private CriteriaQuery<Account> getCriteriaQuerySearchAccount(String search) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = builder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Matcher matcher = Pattern.compile("\\s").matcher(search);
        if (matcher.find()) {
            String[] partitionSearch = search.split("\\s");
            String firstSearch = partitionSearch[0];
            String secondSearch = partitionSearch[1];
            return criteriaQuery.select(root).where
                    (builder.and(builder.like(builder.lower(root.get("name")), "%" + firstSearch + "%"),
                            builder.like(builder.lower(root.get("surname")), "%" + secondSearch + "%")));
        } else {
            return criteriaQuery.select(root).where(builder.or(builder.like(builder.lower(root.get("name")), "%" + search + "%"),
                    builder.like(builder.lower(root.get("surname")), "%" + search + "%")));
        }
    }

    public List<Account> getPageAccounts(String search, int startPage, int maxPage) {
        return entityManager.createQuery(getCriteriaQuerySearchAccount(search)).setFirstResult(startPage).setMaxResults(maxPage).getResultList();
    }

    public int countSearchResultAccount(String search) {
        return entityManager.createQuery(getCriteriaQuerySearchAccount(search)).getResultList().size();
    }
}
