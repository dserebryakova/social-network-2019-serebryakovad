package com.getjavajob.dao.dao;

import java.util.List;

public interface PhoneDao<T, ID> {
    void create(T t);

    T getId(ID id);

    void delete(ID id);

    List<T> getPhonesByOwnerId(ID id);
}
