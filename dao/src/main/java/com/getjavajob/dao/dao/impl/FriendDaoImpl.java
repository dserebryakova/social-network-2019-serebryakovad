package com.getjavajob.dao.dao.impl;

import com.getjavajob.common.Account;
import com.getjavajob.common.Friend;
import com.getjavajob.dao.dao.FriendDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class FriendDaoImpl implements FriendDao<Friend, Long> {
    @PersistenceContext
    private EntityManager entityManager;

    private final AccountDaoImpl accountDaoImpl;

    @Autowired
    public FriendDaoImpl(AccountDaoImpl accountDaoImpl) {
        this.accountDaoImpl = accountDaoImpl;
    }

    public void create(Friend friend) {
        entityManager.persist(friend);
    }

    public void requestFriend(Friend friend) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
        Root<Friend> from = criteriaQuery.from(Friend.class);
        CriteriaQuery<Friend> select = criteriaQuery.select(from).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(from.get("accountMaster"), friend.getAccountMaster()),
                        criteriaBuilder.equal(from.get("accountFriend"), friend.getAccountFriend())));
        Friend currentRel = entityManager.createQuery(select).getSingleResult();
        currentRel.setAccepted(true);
        entityManager.merge(currentRel);
    }

    public Friend getId(Long id) {
        return entityManager.find(Friend.class, id);
    }

    public List<Account> getAllFriendById(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
        Root<Friend> friendRoot = criteriaQuery.from(Friend.class);
        CriteriaQuery<Friend> firstSelect = criteriaQuery.select(friendRoot).where(
                criteriaBuilder.equal(friendRoot.get("accepted"), true),
                criteriaBuilder.or(
                        criteriaBuilder.equal(friendRoot.get("accountMaster"), id),
                        criteriaBuilder.equal(friendRoot.get("accountFriend"), id)));
        List<Friend> friendship = entityManager.createQuery(firstSelect).getResultList();
        Set<Account> result = new HashSet<>();
        for (Friend friend : friendship) {
            if (!friend.getAccountMaster().getId().equals(id)) {
                result.add(friend.getAccountMaster());
            }
            if (!friend.getAccountFriend().getId().equals(id)) {
                result.add(friend.getAccountFriend());
            }
        }
        return new ArrayList<>(result);
    }

    public List<Account> getSubscriptions(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
        Root<Friend> friendRoot = criteriaQuery.from(Friend.class);
        CriteriaQuery<Friend> firstSelect = criteriaQuery.select(friendRoot).where(
                criteriaBuilder.equal(friendRoot.get("accepted"), false),
                criteriaBuilder.equal(friendRoot.get("accountFriend"), accountDaoImpl.getId(id)));
        List<Friend> friendship = entityManager.createQuery(firstSelect).getResultList();
        Set<Account> result = new HashSet<>();
        for (Friend friend : friendship) {
            if (!friend.getAccountMaster().getId().equals(id)) {
                result.add(friend.getAccountMaster());
            }
            if (!friend.getAccountFriend().getId().equals(id)) {
                result.add(friend.getAccountFriend());
            }
        }
        return new ArrayList<>(result);
    }

    public List<Account> getAllReqFriendById(Long id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
        Root<Friend> friendRoot = criteriaQuery.from(Friend.class);
        CriteriaQuery<Friend> firstSelect = criteriaQuery.select(friendRoot).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(friendRoot.get("accepted"), false),
                        criteriaBuilder.equal(friendRoot.get("accountMaster"), accountDaoImpl.getId(id))));
        List<Friend> friendship = entityManager.createQuery(firstSelect).getResultList();
        Set<Account> result = new HashSet<>();
        for (Friend friend : friendship) {
            result.add(friend.getAccountFriend());
        }
        return new ArrayList<>(result);
    }

    public void delete(Friend friend) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
        Root<Friend> friendRoot = criteriaQuery.from(Friend.class);
        Predicate predicateMasterAndFriend = criteriaBuilder.and(
                criteriaBuilder.equal(friendRoot.get("accountMaster"), friend.getAccountMaster()),
                criteriaBuilder.equal(friendRoot.get("accountFriend"), friend.getAccountFriend())
        );
        Predicate predicateFriendAndMaster = criteriaBuilder.and(
                criteriaBuilder.equal(friendRoot.get("accountMaster"), friend.getAccountFriend()),
                criteriaBuilder.equal(friendRoot.get("accountFriend"), friend.getAccountMaster())
        );
        CriteriaQuery<Friend> firstSelect = criteriaQuery.select(friendRoot).where(
                criteriaBuilder.or(
                        predicateMasterAndFriend, predicateFriendAndMaster
                )
        );
        Friend deleteFriend = entityManager.createQuery(firstSelect).getSingleResult();
        entityManager.remove(deleteFriend);
    }

    public boolean checkUsersHaveFriendShip(Long user, Long friend, boolean accepted) {
        try {
            Account accountMaster = accountDaoImpl.getId(user);
            Account accountFriend = accountDaoImpl.getId(friend);
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Friend> criteriaQuery = criteriaBuilder.createQuery(Friend.class);
            Root<Friend> friendRoot = criteriaQuery.from(Friend.class);
            Predicate predicateMasterAndFriend = criteriaBuilder.and(
                    criteriaBuilder.equal(friendRoot.get("accountMaster"), accountMaster),
                    criteriaBuilder.equal(friendRoot.get("accountFriend"), accountFriend)
            );
            Predicate predicateFriendAndMaster = criteriaBuilder.and(
                    criteriaBuilder.equal(friendRoot.get("accountMaster"), accountFriend),
                    criteriaBuilder.equal(friendRoot.get("accountFriend"), accountMaster)
            );
            CriteriaQuery<Friend> firstSelect = criteriaQuery.select(friendRoot).where(
                    criteriaBuilder.and(criteriaBuilder.equal(friendRoot.get("accepted"), accepted),
                            criteriaBuilder.or(
                                    predicateMasterAndFriend, predicateFriendAndMaster
                            )
                    ));

            entityManager.createQuery(firstSelect).getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }
}
