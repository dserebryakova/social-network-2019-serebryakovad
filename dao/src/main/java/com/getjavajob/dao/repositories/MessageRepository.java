package com.getjavajob.dao.repositories;

import com.getjavajob.common.Message;
import com.getjavajob.common.enums.MessageType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {

    @Query(value = "SELECT m FROM Message m WHERE ((m.sender.id = :sender AND m.recipient = :recipient) OR " +
            "(m.sender.id = :recipient AND m.recipient = :sender)) AND m.messageType = 'PRIVATE' ORDER BY m.sendTime")
    List<Message> getAllPrivateMessages(@Param("sender") Long sender, @Param("recipient") Long recipient);

    @Query(value = "SELECT m FROM Message m " +
            "WHERE m.id IN (SELECT MAX(id) FROM Message WHERE messageType = 'PRIVATE' GROUP BY CASE " +
            "WHEN sender.id > recipient THEN recipient ELSE sender END," +
            "CASE WHEN sender.id < recipient THEN recipient ELSE sender END)" +
            "AND (m.sender.id = :id OR m.recipient = :id) ORDER BY m.sendTime DESC")
    List<Message> getLastMessage(@Param("id") Long id);

    List<Message> findByRecipientAndMessageTypeOrderBySendTimeDesc(Long recipient, MessageType messageType);

}
