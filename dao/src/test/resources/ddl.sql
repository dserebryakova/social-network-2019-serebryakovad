DROP TABLE IF EXISTS MESSAGES;
DROP TABLE IF EXISTS GROUP_MEMBERS;
DROP TABLE IF EXISTS FRIEND;
DROP TABLE IF EXISTS GROUPS_;
DROP TABLE IF EXISTS PHONES;
DROP TABLE IF EXISTS ACCOUNT;

CREATE TABLE ACCOUNT
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(45)           NOT NULL,
    surname      VARCHAR(80)           NULL,
    patronymic   VARCHAR(45)           NULL,
    birthdate    VARCHAR(45)           NULL,
    address_home VARCHAR(255)          NULL,
    address_work VARCHAR(255)          NULL,
    email        VARCHAR(45)           NOT NULL,
    password     VARCHAR(255)          NOT NULL,
    reg_date     VARCHAR(45)           NOT NULL,
    icq          VARCHAR(45)           NULL,
    skype        VARCHAR(80)           NULL,
    mood_status  VARCHAR(255)          NULL,
    photo        LONGBLOB              NULL,
    role         VARCHAR(45)           NOT NULL CHECK (ROLE IN ('ADMIN', 'USER')),
    PRIMARY KEY (id),
    UNIQUE (email)
);

CREATE TABLE GROUPS_
(
    id        BIGINT AUTO_INCREMENT NOT NULL,
    name      VARCHAR(45)           NOT NULL,
    reg_date  VARCHAR(45)           NOT NULL,
    id_owner  BIGINT                NOT NULL,
    thematics VARCHAR(45)           NULL,
    info      VARCHAR(255)          NULL,
    photo     LONGBLOB              NULL,
    FOREIGN KEY (id_owner) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE GROUP_MEMBERS
(
    id        BIGINT AUTO_INCREMENT                  NOT NULL,
    id_group  BIGINT                                 NOT NULL,
    id_member BIGINT                                 NOT NULL,
    role      ENUM ('SUBSCRIBER', 'MEMBER', 'MODER') NOT NULL,
    FOREIGN KEY (id_group) REFERENCES GROUPS_ (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (id_member) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)

);

CREATE TABLE FRIEND
(
    id        BIGINT AUTO_INCREMENT NOT NULL,
    id_master BIGINT(11)            NOT NULL,
    id_friend BIGINT(11)            NOT NULL,
    accepted  BOOLEAN,
    FOREIGN KEY (id_master) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (id_friend) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE MESSAGES
(
    id           BIGINT AUTO_INCREMENT                          NOT NULL,
    send_time    VARCHAR(20)                                    NOT NULL,
    id_sender    BIGINT(11)                                     NOT NULL,
    id_recipient BIGINT(11)                                     NOT NULL,
    text         VARCHAR(255),
    picture      LONGBLOB,
    type         ENUM ('PRIVATE', 'ACCOUNT_WALL', 'GROUP_WALL') NOT NULL,
    FOREIGN KEY (id_sender) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE PHONES
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    number         VARCHAR(45)           NOT NULL,
    type           ENUM ('HOME', 'WORK') NOT NULL,
    id_owner_phone BIGINT(11)            NOT NULL,
    FOREIGN KEY (id_owner_phone) REFERENCES ACCOUNT (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);