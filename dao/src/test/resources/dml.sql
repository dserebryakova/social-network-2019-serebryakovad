-- ACCOUNT

INSERT INTO ACCOUNT
VALUES (1, 'Kris', 'Red', 'Jon', '1988-10-05',
        'London', 'NY', '1@1.com', '$2y$12$olzWQERw.bgqIboXS2Us/.hQzVwISfHLnZ9Dqa17ZY1aPfI0lT1jW', '2019-12-01',
        '12342', NULL,
        'Ooooh ^^', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (2, 'Sona', 'Frac', NULL, '1956-01-15',
        'Paris', 'Paris', 'admin@gmail.com', '$2y$12$63xAntxbcfLiuJ/90JqQNeOVDl4e7ZDzR8cJaMVekMF4vhvf9pqn6',
        '2019-11-19', '542453', 'ssomss',
        'Gott ist tot', NULL, 'ROLE_ADMIN');
INSERT INTO ACCOUNT
VALUES (3, 'Carmen', 'Kilt', 'Jane', '1999-07-01',
        'Rome', NULL, '3@3.com', '$2y$12$ajOfMXoONrSdkVqvCxz6WuuZYuEpTb29d9H7FztQOigF7phUopZ4K', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (4, 'Many', 'Lion', 'War', '1999-07-01',
        'Rome', NULL, '4@4.com', '$2y$12$OWoYLew/tTIiDEbVE5AxzOoviZQLzdegMaVYESjQ8rxFGWXwddZ8a', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (5, 'Opra', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '5@5.com', '$2y$12$72PQBB038QAYLW3k9EA8Ues21x5z4p9p55zO0hDKz318sX1IfALSO', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (6, 'Poppy', 'Kol', 'Qass', '1999-07-01',
        'Rome', NULL, '6@6.com', '$2y$12$F.mX78yzSWykd3aGlZuRyuyJVQAA23vkarSwRrZH4AkGc8RItpRlu', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (7, 'Sara', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '7@7.com', '$2y$12$2l3FAT1Q5CCVEpT6qnR6m.AtNLYFgZiLAmeWj/5.IfldW77Ssh2y6', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (8, 'Piter', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '8@8.com', '$2y$12$fKvKLkoYl28tA8cHxyJxtuUJQiHyWrJpEDhpnRsITmWjDEtFUPIau', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (9, 'Das', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '9@9.com', '$2y$12$QO2F0rskX0sddB.t5asIbeQaW95ijtFUGlzT7FZFA5zRYkKOasqly', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (10, 'Oppi', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '10@10.com', '$2y$12$rLAtCKlhEEphSa/Uz7SnJuSNgh7r45UzgHRctYM6CPlB1PhNFX8yy', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');
INSERT INTO ACCOUNT
VALUES (11, 'Amanda', 'Filger', 'Qass', '1999-07-01',
        'Rome', NULL, '11@11.com', '$2y$12$GV7Joo4viTYbk3Y9/81juO6wp8vJOXPRUmgAVksJ4TWbEJM52O2jm', '2019-11-11',
        '3244234', 'yhg_iu',
        'Night....', NULL, 'ROLE_USER');

-- GROUPS_

INSERT INTO GROUPS_
VALUES (1, 'Чудеса природы', '13-12-2019', 1, 'Природа', 'Удивительные природные явления и факты о живой природе',
        null);
INSERT INTO GROUPS_
VALUES (2, 'Японские истребители', '08-09-2018', 2, 'Техника', 'Группа посвящена фанатам японской авиационной техники',
        null);
INSERT INTO GROUPS_
VALUES (3, 'ABBA', '04-08-2019', 4, 'Музыка', 'Фан группа', null);
INSERT INTO GROUPS_
VALUES (4, 'About TV', '09-06-2016', 3, 'TV', null, null);
INSERT INTO GROUPS_
VALUES (5, 'Hollywood Undead', '28-12-2019', 5, 'Сообщество фанатов группы Hollywood Undead', '', null);
INSERT INTO GROUPS_
VALUES (6, 'PREMIER', '01-02-2019', 8, 'Кинотеатр', 'Жизнь кинотеатра Премьер', null);
INSERT INTO GROUPS_
VALUES (7, 'WeLoveCats', '03-11-2019', 1, null, null, null);
INSERT INTO GROUPS_
VALUES (8, '888', '11-11-2016', 7, null, null, null);

-- GROUP_MEMBERS

INSERT INTO GROUP_MEMBERS
VALUES (1, 2, 7, 'SUBSCRIBER');
INSERT INTO GROUP_MEMBERS
VALUES (2, 2, 3, 'MEMBER');
INSERT INTO GROUP_MEMBERS
VALUES (3, 2, 5, 'MEMBER');
INSERT INTO GROUP_MEMBERS
VALUES (4, 2, 9, 'SUBSCRIBER');
INSERT INTO GROUP_MEMBERS
VALUES (5, 2, 4, 'SUBSCRIBER');
INSERT INTO GROUP_MEMBERS
VALUES (6, 2, 8, 'MEMBER');
INSERT INTO GROUP_MEMBERS
VALUES (7, 8, 6, 'MEMBER');
INSERT INTO GROUP_MEMBERS
VALUES (8, 2, 10, 'MODER');

-- FRIEND

INSERT INTO FRIEND
VALUES (1, 3, 1, true);
INSERT INTO FRIEND
VALUES (2, 2, 3, false);
INSERT INTO FRIEND
VALUES (3, 1, 2, true);
INSERT INTO FRIEND
VALUES (4, 2, 4, false);
INSERT INTO FRIEND
VALUES (5, 7, 2, false);
INSERT INTO FRIEND
VALUES (6, 10, 2, false);
INSERT INTO FRIEND
VALUES (7, 2, 8, false);
INSERT INTO FRIEND
VALUES (8, 2, 11, false);

-- MESSAGES

INSERT INTO MESSAGES
VALUES (1, '2018-07-09 10:11:12', 1, 2, 'Привет! Как дела?', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (2, '2018-07-10 10:11:12', 2, 1, 'Привет! Сейчас собираемся на дачу', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (3, '2018-07-11 10:11:12', 3, 1, 'Привет! Как прошел день?', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (4, '2018-07-11 10:12:34', 1, 3, 'Не очень хорошо, много работы', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (5, '2018-07-12 10:11:12', 3, 2, 'Привет! Не хочешь вступить в нашу группу?', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (6, '2018-07-13 10:11:12', 1, 2, 'Чего делаешь?', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (7, '2018-07-14 10:11:12', 2, 1, 'Работаю', NULL, 'PRIVATE');
INSERT INTO MESSAGES
VALUES (8, '2018-07-09 10:11:12', 1, 2, 'Привет! Давно не виделись', NULL, 'ACCOUNT_WALL');
INSERT INTO MESSAGES
VALUES (9, '2018-07-10 10:11:12', 1, 2, 'Я тут', NULL, 'ACCOUNT_WALL');
INSERT INTO MESSAGES
VALUES (10, '2018-07-10 10:11:12', 1, 2, 'Напиши мне', NULL, 'ACCOUNT_WALL');
INSERT INTO MESSAGES
VALUES (11, '2018-07-12 10:11:12', 4, 2, 'Привет! Прими мой запрос!!!', NULL, 'ACCOUNT_WALL');
INSERT INTO MESSAGES
VALUES (12, '2018-07-14 10:11:12', 2, 1, 'Как дела?', NULL, 'ACCOUNT_WALL');
INSERT INTO MESSAGES
VALUES (13, '2018-07-09 10:11:12', 10, 2, 'Объявление для всех участников - в группе технические работы', NULL,
        'GROUP_WALL');
INSERT INTO MESSAGES
VALUES (14, '2018-07-10 10:11:12', 3, 2, 'Печально :(', NULL, 'GROUP_WALL');
INSERT INTO MESSAGES
VALUES (15, '2018-07-10 10:11:12', 5, 2, 'Когда группа будет работать?', NULL, 'GROUP_WALL');
INSERT INTO MESSAGES
VALUES (16, '2018-07-12 10:11:12', 5, 2, 'Админ, прими моего друга!', NULL, 'GROUP_WALL');
INSERT INTO MESSAGES
VALUES (17, '2018-07-14 10:11:12', 6, 8, 'Есть тут кто живой?', NULL, 'GROUP_WALL');

-- PHONES

INSERT INTO PHONES
VALUES (1, '+1 (111) 111-11-11', 'HOME', 2);
INSERT INTO PHONES
VALUES (2, '+2 (222) 222-22-22', 'HOME', 2);
INSERT INTO PHONES
VALUES (3, '+3 (333) 333-33-33', 'WORK', 2);
INSERT INTO PHONES
VALUES (4, '+4 (444) 444-44-44', 'HOME', 1);
INSERT INTO PHONES
VALUES (5, '+5 (555) 555-55-55', 'HOME', 4);
INSERT INTO PHONES
VALUES (6, '+6 (666) 666-66-66', 'HOME', 1);
INSERT INTO PHONES
VALUES (7, '+7 (777) 777-77-77', 'HOME', 1);
INSERT INTO PHONES
VALUES (8, '+8 (888) 888-88-88', 'WORK', 1);
INSERT INTO PHONES
VALUES (9, '+9 (999) 999-99-99', 'WORK', 1);


