package com.getjavajob.dao.dao;

import com.getjavajob.common.Phone;
import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.dao.config.DaoConfigTest;
import com.getjavajob.dao.dao.impl.PhoneDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.getjavajob.common.enums.PhoneType.HOME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfig.class, DaoConfigTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
class PhoneDaoImplTest {
    @Autowired
    private PhoneDaoImpl phoneDaoImpl;

    @Test
    void getPhonesByOwnerId() {
        List<Phone> phones = phoneDaoImpl.getPhonesByOwnerId(1L);
        assertEquals(5, phones.size());
    }

    @Test
    void add() {
        Phone phone = new Phone();
        phone.setNumber("+7(999) 999-99-99");
        phone.setPhoneType(HOME);
        phoneDaoImpl.create(phone);
        List<Phone> phonesForId3 = phoneDaoImpl.getPhonesByOwnerId(3L);
        assertEquals(1, phonesForId3.size());
    }

    @Test
    void delete() {
        phoneDaoImpl.delete(5L);
        assertNull(phoneDaoImpl.getId(5L));
    }
}