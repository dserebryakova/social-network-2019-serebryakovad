package com.getjavajob.dao.dao;

import com.getjavajob.common.Account;
import com.getjavajob.common.enums.Role;
import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.dao.config.DaoConfigTest;
import com.getjavajob.dao.dao.impl.AccountDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfig.class, DaoConfigTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
class AccountDaoImplTest {
    @Autowired
    private AccountDaoImpl accountDaoImpl;

    @Test
    void create() {
        Account account = new Account();
        account.setName("Anna");
        account.setEmail("18@18.com");
        account.setPassword("333");
        account.setRegDate("2019-06-01");
        account.setRole(Role.ROLE_ADMIN);
        accountDaoImpl.create(account);
        Account expected = accountDaoImpl.getAccountByEmail("18@18.com");
        assertEquals("Anna", expected.getName());
    }

    @Test
    void getAll() {
        List<Account> list = accountDaoImpl.getAll();
        assertEquals(11, list.size());
    }

    @Test
    void getId() {
        Account account = accountDaoImpl.getId(1L);
        assertEquals("Kris", account.getName());
    }

    @Test
    void update() {
        Account account = accountDaoImpl.getId(1L);
        account.setName("Vally");
        accountDaoImpl.update(account);
        assertEquals("Vally", account.getName());
    }

    @Test
    void delete() {
        accountDaoImpl.delete(1L);
        assertNull(accountDaoImpl.getId(1L));
    }

    @Test
    void getEmail() {
        Account account = accountDaoImpl.getAccountByEmail("admin@gmail.com");
        assertEquals("Sona", account.getName());
    }

    @Test
    void searchAccounts() {
        List<Account> accounts = accountDaoImpl.getPageAccounts("o", 0, 3);
        assertEquals(2, accounts.size());
    }
}