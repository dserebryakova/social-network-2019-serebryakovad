package com.getjavajob.dao.dao;

import com.getjavajob.common.Account;
import com.getjavajob.common.Friend;
import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.dao.config.DaoConfigTest;
import com.getjavajob.dao.dao.impl.AccountDaoImpl;
import com.getjavajob.dao.dao.impl.FriendDaoImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfig.class, DaoConfigTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class FriendDaoImplTest {
    @Autowired
    private FriendDaoImpl friendDaoImpl;

    @Autowired
    private AccountDaoImpl accountDaoImpl;

    private Account accountMaster;
    private Account accountFriend2;
    private Account accountFriend3;
    private Friend expected;
    private Friend expected1;

    @BeforeEach
    void setUp() {
        accountMaster = accountDaoImpl.getId(6L);
        Account accountFriend = accountDaoImpl.getId(2L);
        accountFriend2 = accountDaoImpl.getId(3L);
        accountFriend3 = accountDaoImpl.getId(4L);
        expected = new Friend(accountMaster, accountFriend);
        expected1 = new Friend(accountFriend3, accountFriend2);
        Friend expected2 = new Friend(accountMaster, accountFriend2);
        friendDaoImpl.create(expected);
        friendDaoImpl.create(expected2);
    }

    @Test
    void create() {
        friendDaoImpl.create(expected1);
        List<Account> friend = friendDaoImpl.getAllReqFriendById(accountFriend3.getId());
        assertEquals(1, friend.size());
    }

    @Test
    void getIdFriend() {
        List<Account> friend = friendDaoImpl.getAllFriendById(1L);
        assertEquals(3, friend.size());
    }

    @Test
    void delete() {
        friendDaoImpl.delete(expected);
        List<Account> friend = friendDaoImpl.getAllReqFriendById(accountMaster.getId());
        assertEquals(2, friend.size());
    }

    @Test
    void reqFriend() {
        friendDaoImpl.requestFriend(expected);
        List<Account> friend = friendDaoImpl.getAllFriendById(accountMaster.getId());
        assertEquals(1, friend.size());
    }

    @Test
    void getSubscriptions() {
        List<Account> subs = friendDaoImpl.getSubscriptions(accountFriend2.getId());
        assertEquals(2, subs.size());
    }

    @Test
    void checkUsersHaveFriendShip() {
        boolean friendship = friendDaoImpl.checkUsersHaveFriendShip(2L, 7L, false);
        assertTrue(friendship);
    }
}