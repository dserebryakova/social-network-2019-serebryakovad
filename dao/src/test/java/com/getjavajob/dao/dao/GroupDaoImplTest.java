package com.getjavajob.dao.dao;

import com.getjavajob.common.Account;
import com.getjavajob.common.Group;
import com.getjavajob.common.GroupMember;
import com.getjavajob.common.enums.GroupRole;
import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.dao.config.DaoConfigTest;
import com.getjavajob.dao.dao.impl.AccountDaoImpl;
import com.getjavajob.dao.dao.impl.GroupDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.getjavajob.common.enums.GroupRole.MEMBER;
import static com.getjavajob.common.enums.GroupRole.MODER;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfig.class, DaoConfigTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
class GroupDaoImplTest {
    @Autowired
    private AccountDaoImpl accountDaoImpl;

    @Autowired
    private GroupDaoImpl groupDaoImpl;

    @Test
    void create() {
        Account account = accountDaoImpl.getId(1L);
        Group group = new Group();
        group.setName("Nunu");
        group.setIdOwner(account.getId());
        group.setRegDate("2020-01-01");
        groupDaoImpl.create(group);
        assertEquals(3, groupDaoImpl.getByIdOwner(1L).size());
    }

    @Test
    void insertMember() {
        groupDaoImpl.insertMember(1L, 5L);
    }

    @Test
    void getAll() {
        List<Group> groups = groupDaoImpl.getAll();
        assertEquals(8, groups.size());
    }

    @Test
    void getId() {
        Group group = groupDaoImpl.getId(3L);
        assertEquals("ABBA", group.getName());
    }

    @Test
    void getAllMemberFromGrMem() {
        List<GroupMember> members = groupDaoImpl.getAllMemberFromGrMem(8L);
        assertEquals(1, members.size());
    }

    @Test
    void getByIdOwner() {
        List<Group> groups = groupDaoImpl.getByIdOwner(1L);
        assertEquals(2, groups.size());
    }

    @Test
    void getMembership() {
        List<Group> groups = groupDaoImpl.getMembership(3L);
        assertEquals(1, groups.size());
    }

    @Test
    void getMembers() {
        List<GroupMember> members = groupDaoImpl.getMembers(2L);
        assertEquals(4, members.size());
    }

    @Test
    void getSubscribes() {
        List<GroupMember> subs = groupDaoImpl.getSubscribes(2L);
        assertEquals(3, subs.size());
    }

    @Test
    void update() {
        Group group = groupDaoImpl.getId(3L);
        group.setName("Nature");
        groupDaoImpl.update(group);
        assertEquals("Nature", groupDaoImpl.getId(3L).getName());
    }

    @Test
    void updateModer() {
        groupDaoImpl.updateModer(5L);
        GroupMember groupMember = groupDaoImpl.getGroupMemberById(5L);
        assertEquals(MODER, groupMember.getGroupRole());
    }

    @Test
    void updateMemeber() {
        groupDaoImpl.updateMember(8L);
        GroupMember groupMember = groupDaoImpl.getGroupMemberById(8L);
        assertEquals(MEMBER, groupMember.getGroupRole());
    }

    @Test
    void delete() {
        groupDaoImpl.delete(1L);
        assertNull(groupDaoImpl.getId(1L));
    }

    @Test
    void deleteMemeber() {
        groupDaoImpl.deleteMember(8L, 6L);
        assertNull(groupDaoImpl.getGroupMemberById(7L));
    }

    @Test
    void getRole() {
        GroupRole groupRoleNull = groupDaoImpl.getRole(2L, 6L);
        GroupRole groupRoleModer = groupDaoImpl.getRole(2L, 10L);
        GroupRole groupRoleMemeber = groupDaoImpl.getRole(8L, 6L);
        assertNull(groupRoleNull);
        assertEquals(MODER, groupRoleModer);
        assertEquals(MEMBER, groupRoleMemeber);
    }

    @Test
    void searchGroup() {
        List<Group> groups = groupDaoImpl.searchGroup("o");
        assertEquals(3, groups.size());
    }

    @Test
    void getPageGroup() {
        List<Group> groups = groupDaoImpl.getPageGroup("r", 0, 3);
        System.out.println(groups.size());
        for (Group group : groups) {
            System.out.println(group.getName());
        }
    }

    @Test
    void getInfoOwner() {
        Account owner = groupDaoImpl.getInfoOwner(2L, 2L);
        assertEquals("Sona", owner.getName());
    }

    @Test
    void checkUsersHaveMemberShip() {
        boolean membershipTrue = groupDaoImpl.checkUsersHaveMemberShip(2L, 8L);
        boolean membershipFalse = groupDaoImpl.checkUsersHaveMemberShip(2L, 6L);
        assertTrue(membershipTrue);
        assertFalse(membershipFalse);
    }
}