package com.getjavajob.dao.repositories;

import com.getjavajob.common.Message;
import com.getjavajob.common.enums.MessageType;
import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.dao.config.DaoConfigTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoConfig.class, DaoConfigTest.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
class MessageRepositoryTest {
    @Autowired
    private MessageRepository messageRepository;

    @Test
    void getAllPrivateMessages() {
        List<Message> privateMessages = messageRepository.getAllPrivateMessages(2L, 1L);
        assertEquals(5, privateMessages.size());
    }

    @Test
    void getLastMessage() {
        List<Message> allLastMessages = messageRepository.getLastMessage(2L);
        assertEquals(2, allLastMessages.size());
    }

    @Test
    void findByRecipientAndMessageTypeOrderBySendTimeDesc() {
        List<Message> wallAccountMessage = messageRepository.findByRecipientAndMessageTypeOrderBySendTimeDesc(2L, MessageType.ACCOUNT_WALL);
        assertEquals(3, wallAccountMessage.size());
        List<Message> wallGroupMessage = messageRepository.findByRecipientAndMessageTypeOrderBySendTimeDesc(8L, MessageType.GROUP_WALL);
        assertEquals(1, wallGroupMessage.size());
    }
}