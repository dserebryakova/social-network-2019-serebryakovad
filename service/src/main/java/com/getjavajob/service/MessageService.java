package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.common.Message;
import com.getjavajob.common.dto.MessageChatDto;
import com.getjavajob.common.enums.MessageType;
import com.getjavajob.dao.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MessageService {
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Transactional
    public void addMessage(Message message) {
        message.setSendTime(getDate());
        messageRepository.save(message);
    }

    @Transactional
    public void addChatMessage(MessageChatDto messageChatDto, Account account) {
        Message message = new Message();
        message.setSender(account);
        message.setRecipient(messageChatDto.getRecipient());
        message.setText(messageChatDto.getText());
        message.setMessageType(MessageType.PRIVATE);
        message.setSendTime(getDate());
        messageRepository.save(message);
    }

    public List<Message> getMessages(Long sender, Long recipient) {
        return messageRepository.getAllPrivateMessages(sender, recipient);
    }

    public List<Message> getMessageForWall(Long recipient, MessageType messageType) {
        return messageRepository.findByRecipientAndMessageTypeOrderBySendTimeDesc(recipient, messageType);
    }

    public byte[] getPhoto(Long id) {
        return messageRepository.findById(id).get().getPicture();
    }

    public List<Message> getLastMessages(Long sender) {
        return messageRepository.getLastMessage(sender);
    }

    @Transactional
    public void delete(Long id) {
        messageRepository.deleteById(id);
    }

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
