package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.dao.dao.impl.AccountDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {
    private final AccountDaoImpl accountDaoImpl;

    @Autowired
    public AccountService(AccountDaoImpl accountDaoImpl) {
        this.accountDaoImpl = accountDaoImpl;
    }

    @Transactional
    public void createAccount(Account account) {
        accountDaoImpl.create(account);
    }

    @Transactional
    public void updateAccount(Account account) {
        accountDaoImpl.update(account);
    }

    @Transactional
    public void deleteAccount(Long id) {
        accountDaoImpl.delete(id);
    }

    public List<Account> getAllAccounts() {
        return accountDaoImpl.getAll();
    }

    public List<Account> searchAccounts(String search) {
        return accountDaoImpl.searchAccounts(search);
    }

    public List<Account> getPageAccounts(String search, int startPage, int maxPage) {
        return accountDaoImpl.getPageAccounts(search, startPage, maxPage);
    }

    public Account getAccountByEmail(String email) {
        return accountDaoImpl.getAccountByEmail(email);
    }

    public boolean checkEmail(String email) {
        return accountDaoImpl.getAccountByEmail(email) != null;
    }

    public Account getById(Long id) {
        return accountDaoImpl.getId(id);
    }

    public int countSearchResultAccount(String search) {
        return accountDaoImpl.countSearchResultAccount(search);
    }

    public byte[] getPhoto(Long id) {
        return accountDaoImpl.getPhoto(id);
    }
}
