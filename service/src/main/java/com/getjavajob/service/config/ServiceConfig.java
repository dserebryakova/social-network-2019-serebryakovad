package com.getjavajob.service.config;

import com.getjavajob.dao.config.DaoConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("com.getjavajob.service")
@Import(DaoConfig.class)
@EnableTransactionManagement
public class ServiceConfig {
}
