package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.common.Group;
import com.getjavajob.common.GroupMember;
import com.getjavajob.common.enums.GroupRole;
import com.getjavajob.dao.dao.impl.GroupDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupService {
    private final GroupDaoImpl groupDaoImpl;

    @Autowired
    public GroupService(GroupDaoImpl groupDaoImpl) {
        this.groupDaoImpl = groupDaoImpl;
    }

    @Transactional
    public void createGroup(Group group) {
        groupDaoImpl.create(group);
    }

    @Transactional
    public void insertMember(Long idGroup, Long idMember) {
        groupDaoImpl.insertMember(idGroup, idMember);
    }

    public Account getInfoOwner(Long idGroup, Long idMember) {
        return groupDaoImpl.getInfoOwner(idGroup, idMember);
    }

    @Transactional
    public void updateGroup(Group group) {
        groupDaoImpl.update(group);
    }

    @Transactional
    public void updateMember(Long id) {
        groupDaoImpl.updateMember(id);
    }

    @Transactional
    public void updateModer(Long id) {
        groupDaoImpl.updateModer(id);
    }

    @Transactional
    public void deleteGroup(Long id) {
        groupDaoImpl.delete(id);
    }

    @Transactional
    public void deleteMember(Long idGroup, Long idMember) {
        groupDaoImpl.deleteMember(idGroup, idMember);
    }

    public boolean checkMembership(Long idGroup, Long idMember) {
        return groupDaoImpl.checkUsersHaveMemberShip(idGroup, idMember);
    }

    public List<Group> getAllGroups() {
        return groupDaoImpl.getAll();
    }

    public List<GroupMember> getAllSubs(Long idGroup) {
        return groupDaoImpl.getSubscribes(idGroup);
    }

    public List<Group> searchGroup(String search) {
        return groupDaoImpl.searchGroup(search);
    }

    public List<Group> getPageGroup(String search, int startPage, int maxPage) {
        return groupDaoImpl.getPageGroup(search, startPage, maxPage);
    }

    public int countSearchResultGroup(String search) {
        return groupDaoImpl.countSearchResultGroup(search);
    }

    public Group getById(Long id) {
        return groupDaoImpl.getId(id);
    }

    public GroupRole getRole(Long idGroup, Long idMember) {
        return groupDaoImpl.getRole(idGroup, idMember);
    }

    public List<GroupMember> getAllMembersFromGrMem(Long id) {
        return groupDaoImpl.getAllMemberFromGrMem(id);
    }

    public List<Group> getByIdOwner(Long id) {
        return groupDaoImpl.getByIdOwner(id);
    }

    public List<Group> getMembership(Long id) {
        return groupDaoImpl.getMembership(id);
    }

    public List<GroupMember> getAllMembers(Long id) {
        return groupDaoImpl.getMembers(id);
    }

    public byte[] getPhoto(Long id) {
        return groupDaoImpl.getPhoto(id);
    }

    public byte[] getOldPhoto(Long id) {
        return groupDaoImpl.getPhoto(id);
    }
}
