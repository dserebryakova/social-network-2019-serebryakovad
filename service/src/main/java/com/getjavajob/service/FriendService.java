package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.common.Friend;
import com.getjavajob.dao.dao.impl.FriendDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FriendService {
    private final FriendDaoImpl friendDaoImpl;

    @Autowired
    public FriendService(FriendDaoImpl friendDaoImpl) {
        this.friendDaoImpl = friendDaoImpl;
    }

    @Transactional
    public void requestFriend(Friend friend) {
        friendDaoImpl.create(friend);
    }

    @Transactional
    public void approveFriend(Friend friend) {
        friendDaoImpl.requestFriend(friend);
    }

    @Transactional
    public void deleteFriend(Friend friend) {
        friendDaoImpl.delete(friend);
    }

    public List<Account> getFriends(Long id) {
        return friendDaoImpl.getAllFriendById(id);
    }

    public List<Account> getReqFriends(Long id) {
        return friendDaoImpl.getAllReqFriendById(id);
    }

    public List<Account> getSubscriptions(Long id) {
        return friendDaoImpl.getSubscriptions(id);
    }

    public boolean checkUsersHaveFriendShip(Long user, Long friend, boolean accepted) {
        return friendDaoImpl.checkUsersHaveFriendShip(user, friend, accepted);
    }
}
