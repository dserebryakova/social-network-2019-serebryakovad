package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.common.Friend;
import com.getjavajob.dao.dao.impl.FriendDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FriendServiceTest {
    @Mock
    private FriendDaoImpl friendDaoImpl;

    @InjectMocks
    private FriendService friendService;

    private final Account friendFirst = new Account();
    private final Account friendSecond = new Account();
    private final Friend friendship = new Friend(friendFirst, friendSecond);

    @Test
    void requestFriend() {
        friendDaoImpl.create(friendship);
        verify(friendDaoImpl).create(friendship);
    }

    @Test
    void approveFriend() {
        friendDaoImpl.requestFriend(friendship);
        verify(friendDaoImpl).requestFriend(friendship);
    }

    @Test
    void deleteFriend() {
        friendDaoImpl.delete(friendship);
        verify(friendDaoImpl).delete(friendship);
    }

    @Test
    void getFriends() {
        List<Account> accounts = new ArrayList<>();
        when(friendDaoImpl.getAllFriendById(friendSecond.getId())).thenReturn(accounts);
        friendService.getFriends(friendSecond.getId());
        verify(friendDaoImpl).getAllFriendById(friendSecond.getId());
    }

    @Test
    void getReqFriends() {
        List<Account> accounts = new ArrayList<>();
        when(friendDaoImpl.getAllReqFriendById(friendSecond.getId())).thenReturn(accounts);
        friendService.getReqFriends(friendSecond.getId());
        verify(friendDaoImpl).getAllReqFriendById(friendSecond.getId());
    }
}