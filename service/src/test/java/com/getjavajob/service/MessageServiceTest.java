package com.getjavajob.service;

import com.getjavajob.common.Message;
import com.getjavajob.common.dto.MessageChatDto;
import com.getjavajob.dao.repositories.MessageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.common.enums.MessageType.ACCOUNT_WALL;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {
    @Mock
    private MessageRepository messageRepository;

    @InjectMocks
    private MessageService messageService;

    private final Message message = new Message();
    private final MessageChatDto messageChatDto = new MessageChatDto();


    @Test
    void addMessage() {
        messageService.addMessage(message);
        verify(messageRepository).save(message);
    }

    @Test
    void getMessages() {
        List<Message> messages = new ArrayList<>();
        when(messageRepository.getAllPrivateMessages(1L, 2L)).thenReturn(messages);
        messageService.getMessages(1L, 2L);
        verify(messageRepository).getAllPrivateMessages(1L, 2L);
    }

    @Test
    void getMessageForWall() {
        List<Message> wallMessages = new ArrayList<>();
        when(messageRepository.findByRecipientAndMessageTypeOrderBySendTimeDesc(1L, ACCOUNT_WALL))
                .thenReturn(wallMessages);
        messageService.getMessageForWall(1L, ACCOUNT_WALL);
        verify(messageRepository).findByRecipientAndMessageTypeOrderBySendTimeDesc(1L, ACCOUNT_WALL);
    }

    @Test
    void getLastMessages() {
        List<Message> lastMessages = new ArrayList<>();
        when(messageRepository.getLastMessage(1L))
                .thenReturn(lastMessages);
        messageService.getLastMessages(1L);
        verify(messageRepository).getLastMessage(1L);
    }

    @Test
    void delete() {
        message.setId(1L);
        messageService.delete(1L);
        verify(messageRepository).deleteById(message.getId());
    }
}