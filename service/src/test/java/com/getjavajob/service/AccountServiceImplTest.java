package com.getjavajob.service;

import com.getjavajob.common.Account;
import com.getjavajob.dao.dao.impl.AccountDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {
    @Mock
    private AccountDaoImpl accountDaoImpl;

    @InjectMocks
    private AccountService accountService;

    private final Account account = new Account();

    @Test
    void createAccount() {
        accountService.createAccount(account);
        verify(accountDaoImpl).create(account);
    }

    @Test
    void updateAccount() {
        accountService.updateAccount(account);
        verify(accountDaoImpl).update(account);
    }

    @Test
    void deleteAccount() {
        account.setId(1L);
        accountService.deleteAccount(1L);
        verify(accountDaoImpl).delete(account.getId());
    }

    @Test
    void getAllAccounts() {
        List<Account> accounts = new ArrayList<>();
        when(accountDaoImpl.getAll()).thenReturn(accounts);
        accountService.getAllAccounts();
        verify(accountDaoImpl).getAll();
    }

    @Test
    void searchAccounts() {
        String search = "g";
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        when(accountDaoImpl.searchAccounts(search)).thenReturn(accounts);
        accountService.searchAccounts(search);
        verify(accountDaoImpl).searchAccounts(search);
    }

    @Test
    void getAccountByEmail() {
        accountService.getAccountByEmail("t87@h.com");
        verify(accountDaoImpl).getAccountByEmail("t87@h.com");
    }
}