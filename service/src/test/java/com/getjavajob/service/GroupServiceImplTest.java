package com.getjavajob.service;

import com.getjavajob.common.Group;
import com.getjavajob.common.GroupMember;
import com.getjavajob.dao.dao.impl.GroupDaoImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {
    @Mock
    private GroupDaoImpl groupDaoImpl;

    @InjectMocks
    private GroupService groupService;

    private final Group group = new Group();

    @Test
    void createGroup() {
        groupService.createGroup(group);
        verify(groupDaoImpl).create(group);
    }

    @Test
    void insertMember() {
        groupService.insertMember(5L, 4L);
        verify(groupDaoImpl).insertMember(5L, 4L);
    }

    @Test
    void getInfoOwner() {
        groupService.getInfoOwner(1L, 2L);
        verify(groupDaoImpl).getInfoOwner(1L, 2L);
    }

    @Test
    void updateGroup() {
        groupService.updateGroup(group);
        verify(groupDaoImpl).update(group);
    }

    @Test
    void updateMember() {
        groupService.updateMember(2L);
        verify(groupDaoImpl).updateMember(2L);
    }

    @Test
    void updateModer() {
        groupService.updateModer(5L);
        verify(groupDaoImpl).updateModer(5L);
    }

    @Test
    void deleteGroup() {
        groupService.deleteGroup(group.getId());
        verify(groupDaoImpl).delete(group.getId());
    }

    @Test
    void deleteMember() {
        groupService.deleteMember(6L, 1L);
        verify(groupDaoImpl).deleteMember(6L, 1L);
    }

    @Test
    void checkMembership() {
        groupService.checkMembership(3L, 3L);
        verify(groupDaoImpl).checkUsersHaveMemberShip(3L, 3L);
    }

    @Test
    void getAllGroups() {
        List<Group> groups = new ArrayList<>();
        when(groupDaoImpl.getAll()).thenReturn(groups);
        groupService.getAllGroups();
        verify(groupDaoImpl).getAll();
    }

    @Test
    void getAllSubs() throws Exception {
        List<GroupMember> subs = new ArrayList<>();
        when(groupDaoImpl.getSubscribes(5L)).thenReturn(subs);
        groupService.getAllSubs(5L);
        verify(groupDaoImpl).getSubscribes(5L);
    }

    @Test
    void searchGroup() {
        List<Group> groups = new ArrayList<>();
        when(groupDaoImpl.searchGroup("we")).thenReturn(groups);
        groupService.searchGroup("we");
        verify(groupDaoImpl).searchGroup("we");
    }

    @Test
    void getById() {
        groupDaoImpl.getId(3L);
        verify(groupDaoImpl).getId(3L);
    }

    @Test
    void getByIdOwner() {
        groupDaoImpl.getInfoOwner(3L, 4L);
        verify(groupDaoImpl).getInfoOwner(3L, 4L);
    }
}