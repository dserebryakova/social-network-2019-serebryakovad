jQuery.validator.addMethod("checkMask", function (value) {
    return /\+\d{1}\ \(\d{3}\)\ \d{3}-\d{2}-\d{2}/g.test(value);
}, "Введите телефон в формате + X (XXX) XXX-XX-XX");

$(function () {
    $.validator.addClassRules({
        'mask-phone': {
            checkMask: true
        }
    });

    $('#updateForm').validate({
        errorElement: "p",
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent("div"));
        },
        submitHandler: function (form) {
            if (!confirm("Вы действительно хотите сохранить изменения?")) {
                return false;
            }
            form.submit();
            return false;
        }
    });

    $(document.body).on('click', '.btn-remove-phone', function () {
        $(this).closest('.phone-input').next('.error').remove();
        $(this).closest('.phone-input').remove();
    });

    $(document).ready(function () {
        $('.mask-phone').mask("+9 (999) 999-99-99", {autoclear: false});
    });

    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true
    });


    $('.btn-add-phone').click(function () {
        let index = $('.phone-input').length + 1;

        let newPhone = $('<div class="input-group phone-input">' +
            '<div class="input-group-btn">' +
            '<select class="btn btn-default dropdown-toggle" name="phones[' + index + '].phoneType" aria-expanded="false">' +
            '<option value="HOME" selected>Домашний</option>' +
            '<option value="WORK">Рабочий</option> ' +
            '</select> ' +
            '</div> ' +
            '<input type="text" name="phones[' + index + '].number" class="mask-phone form-control" value="" />' +
            '<span class="input-group-btn">' +
            '<button class="btn btn-danger btn-remove-phone m8" type="button">' +
            '<span class="glyphicon glyphicon-minus"></span></button></span></div>');
        $('.mask-phone', newPhone).mask("+9 (999) 999-99-99", {autoclear: false});

        $('.phones').append(newPhone);
    });

    $("#input_xml").change(function () {
        let fileExtension = ['xml'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
            $("#warning").html("Файл должен быть только в формате .xml").css('color', 'red');
            $("#input_xml").val("");
        } else {
            $("#warning").html("");
        }
    });

    $("#xmlForm").submit(function () {
        const valFile = $("#input_xml").val();
        if (valFile === "") {
            $("#warning").html("Вы не выбрали файл").css('color', 'red');
            return false;
        } else {
            return true;
        }
    });
});
