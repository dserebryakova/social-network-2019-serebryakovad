$(function () {
    $("#searchFilter").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/filterSearch',
                data: {
                    filter: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        },
        minLength: 1
    });
});