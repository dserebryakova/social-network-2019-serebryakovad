let pageAccount = 1;
const totalRowsAccount = $("#countAccount").text();

let pageGroup = 1;
const totalRowsGroup = $("#countGroup").text();

const rowsPerPage = 3;
const filter = $("#filter").text();

$(function () {
    $.ajax({
        type: "GET",
        url: "/accountPerPage?filter=" + filter + "&page=" + pageAccount,
        dataType: "json",
        success: function (data) {
            let templateRowAcc = $('#templateRowAcc');
            $.each(data, function (i, obj) {
                const rowAcc = templateRowAcc.clone().removeAttr('id').attr('id', 'account' + i).show();
                rowAcc.find('#selectedAcc').html(
                    "<a href=\"home?id=" + obj.id + "\">" +
                    '<img width="80" height="80" class="round" src="/image?action=account&id=' + obj.id + '" alt="deq">' +
                    obj.name + " " + obj.surname + "</a>" +
                    '</div></div>');

                $('#accountsSearch').append(rowAcc + '<br>');
            });
            buttonPrevAcc();
            buttonNextAcc();
        }
    });

    $.ajax({
        type: "GET",
        url: "/groupPerPage?filter=" + filter + "&page=" + pageGroup,
        dataType: "json",
        success: function (data) {
            let templateRowGr = $('#templateRowGr');
            $.each(data, function (i, obj) {
                const rowGr = templateRowGr.clone().removeAttr('id').attr('id', 'group' + i).show();
                rowGr.find('#selectedGr').html(
                    "<a href=\"homeGroup?id=" + obj.id + "\">" +
                    '<img width="80" height="80" class="round" src="/image?action=group&id=' + obj.id + '" alt="deq">' +
                    obj.name + "</a>" +
                    '</div></div>');
                $('#groupSearch').append(rowGr + '<br>');
            });
            buttonPrevGr();
            buttonNextGr();
        }
    });
});

function getAccounts() {
    $.ajax({
        type: "GET",
        url: "/accountPerPage?filter=" + filter + "&page=" + pageAccount,
        dataType: "json",
        success: function (data) {
            let currentCount = 0;
            $.each(data, function (i, obj) {
                $('#account' + i).show().find('#selectedAcc').html(
                    "<a href=\"home?id=" + obj.id + "\">" +
                    '<img width="80" height="80" class="round" src="/image?action=account&id=' + obj.id + '" alt="deq">' +
                    obj.name + " " + obj.surname + "</a>");
                currentCount = i;
            });
            while (currentCount < 2) {
                currentCount++;
                $('#account' + currentCount).hide();
            }
        }
    });
}

$('#prevAcc').on('click', function (e) {
    e.preventDefault();
    pageAccount--;
    buttonPrevAcc();
    buttonNextAcc();
    getAccounts()
});

$('#nextAcc').on('click', function (e) {
    e.preventDefault();
    pageAccount++;
    buttonPrevAcc();
    buttonNextAcc();
    getAccounts()
});

function buttonPrevAcc() {
    if (pageAccount > 1)
        $('#prevAcc').show();
    else
        $('#prevAcc').hide();
}

function buttonNextAcc() {
    let currRow = pageAccount * rowsPerPage;
    if (totalRowsAccount === undefined || currRow > totalRowsAccount)
        $('#nextAcc').hide();
    else
        $('#nextAcc').show();
}

function getGroups() {
    $.ajax({
        type: "GET",
        url: "/groupPerPage?filter=" + filter + "&page=" + pageGroup,
        dataType: "json",
        success: function (data) {
            let currentCount = 0;
            $.each(data, function (i, obj) {
                $('#group' + i).show().find('#selectedGr').html(
                    "<a href=\"homeGroup?id=" + obj.id + "\">" +
                    '<img width="80" height="80" class="round" src="/image?action=group&id=' + obj.id + '" alt="deq">' +
                    obj.name + "</a>");
                currentCount = i;
            });
            while (currentCount < 2) {
                currentCount++;
                $('#group' + currentCount).hide();
            }
        }
    });
}

$('#prevGr').on('click', function (e) {
    e.preventDefault();
    pageGroup--;
    buttonPrevGr();
    buttonNextGr();
    getGroups()
});

$('#nextGr').on('click', function (e) {
    e.preventDefault();
    pageGroup++;
    buttonPrevGr();
    buttonNextGr();
    getGroups()
});

function buttonPrevGr() {
    if (pageGroup > 1)
        $('#prevGr').show();
    else
        $('#prevGr').hide();
}

function buttonNextGr() {
    const currRow = pageGroup * rowsPerPage;
    if (totalRowsGroup === undefined || currRow > totalRowsGroup)
        $('#nextGr').hide();
    else
        $('#nextGr').show();
}