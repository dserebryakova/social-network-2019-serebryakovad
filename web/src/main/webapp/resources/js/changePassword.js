$(document).ready(function () {

    $('#password').keyup(function () {
        const password = $('#password').val();
        if (checkStrength(password) === false) {
            $('#sign-up').attr('disabled', true);
        }
    });

    $('#confirmPassword').blur(function () {
        if ($('#password').val() !== $('#confirmPassword').val()) {
            $('#popover-cpassword').removeClass('hide');
            $('#sign-up').attr('disabled', true);
        } else {
            $('#popover-cpassword').addClass('hide');
        }
    });

    function checkStrength(password) {
        let strength = 0;

        if (password.length > 3) {
            strength += 1;
            $('.eight-character').addClass('text-success');
            $('.eight-character i').removeClass('fa-times').addClass('fa-check');
            $('#popover-password-top').addClass('hide');

        } else {
            $('.eight-character').removeClass('text-success');
            $('.eight-character i').addClass('fa-times').removeClass('fa-check');
            $('#popover-password-top').removeClass('hide');
        }

        if (strength < 2) {
            $('#result').removeClass()
            $('#password-strength').addClass('progress-bar-danger');

            $('#result').addClass('text-danger').text('Very Week');
            $('#password-strength').css('width', '10%');
        } else if (strength == 2) {
            $('#result').addClass('good');
            $('#password-strength').removeClass('progress-bar-danger');
            $('#password-strength').addClass('progress-bar-warning');
            $('#result').addClass('text-warning').text('Week')
            $('#password-strength').css('width', '60%');
            return 'Week'
        } else if (strength == 3) {
            $('#result').removeClass()
            $('#result').addClass('strong');
            $('#password-strength').removeClass('progress-bar-warning');
            $('#password-strength').addClass('progress-bar-success');
            $('#result').addClass('text-success').text('Strength');
            $('#password-strength').css('width', '100%');

            return 'Strong'
        }

    }

    $("#validateForm").validate({
        rules: {
            password: {
                minlength: 4,
            },
            confirmPassword: {
                minlength: 4,
                equalTo: "#password"
            }

        },
        errorPlacement: function (error, element) {
            return true;
        },
        submitHandler: function (form) {
            if (!confirm("Вы действительно хотите сохранить изменения?")) {
                return false;
            }
            form.submit();
            return false;
        }

    });
});