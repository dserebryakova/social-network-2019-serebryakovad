let stompClient = null;
const sender = $('#idSender').text();
const recipient = $('#idRecipient').text();
let count = 0;

$(function () {
    connect();
    scrollBottom();
});

$(function () {
    $('#newMessage').on('click', function () {
        const text = $('#textMessage').val();
        stompClient.send('/chat/sendMessage/' + recipient, {},
            JSON.stringify({'sender': sender, 'recipient': recipient, 'text': text}));
        $('#textMessage').val('');
    });
});

function connect() {
    disconnect();
    var socket = new SockJS('/sendMessage');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/topic/' + recipient, function (message) {
            appendMessage(JSON.parse(message.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
}

function appendMessage(message) {
    let templateRowMess = $('#templateRowMess');
    const rowMess = templateRowMess.clone().removeAttr('id').attr('id', 'mess' + count).show();
    rowMess.find('#selectedMess').html(
        '<img src="/image?action=account&id=' + message.sender + '" width="70" height="70" class="rounded-circle" alt="user">' +
        '<div class="media-body">' +
        '<div class="d-flex align-items-center justify-content-between mb-1">' +
        '<a href="/home?id=' + message.sender + '">' +
        '<h4 class="mb-0">' + message.nameSender + " " + message.surnameSender + '</h4>' +
        '</a>' +
        '</div>' +
        '<div class="bg-primary rounded py-2 px-3 mb-2">' +
        '<p class="text-small mb-0 text-white">' + message.text + '</p>' +
        '</div>' +
        '<p class="small text-muted">' + message.sendTime + '</p>' +
        '</div>');
    $('#formChat').append(rowMess);
    scrollBottom();
    count++;
}

function scrollBottom() {
    $('#formChat').animate({
        scrollTop: $('#formChat')[0].scrollHeight
    }, "slow");
}