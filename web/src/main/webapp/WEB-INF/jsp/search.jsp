<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/friendsAndGroups.css">
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css"/>
    <title>Результаты поиска</title>
</head>

<body>
<div>
    <div>
        <h1>Результаты поиска по: </h1>
        <div id="filter" class="search"><h1><i>${search}</i></h1></div>
    </div>
    <h2>Пользователи</h2>
    <div class="accountsPerPage">
        <div id="accountsSearch" class="panel-back">
            <div id="countAccount" style="display: none;"><p>${countAccount}</p></div>
            <div id="templateRowAcc" style="display:none;">
                <div id="selectedAcc"></div>
            </div>
        </div>
        <ul class="pager">
            <li class="page-item" id="prevAcc" style='display:none'>
                <a class="page-link" href="#" tabindex="-1"><span aria-hidden="true">←</span>Предыдущая</a>
            </li>
            <li class="page-item" id="nextAcc" style='display:none'>
                <a class="page-link" href="#">Следующая<span aria-hidden="true">→</span></a>
            </li>
        </ul>
    </div>
    <c:if test="${countAccount == 0}">
        <h4>Поиск не дал результатов</h4>
    </c:if>

    <h2>Группы</h2>
    <div class="groupsPerPage">
        <div id="groupSearch" class="panel-back">
            <div id="countGroup" style="display: none;"><p>${countGroup}</p></div>
            <div id="templateRowGr" style="display:none;">
                <div id="selectedGr"></div>
            </div>
        </div>
        <ul class="pager">
            <li class="page-item" id="prevGr" style='display:none'>
                <a class="page-link" href="#" tabindex="-1"><span aria-hidden="true">←</span>Предыдущая</a>
            </li>
            <li class="page-item" id="nextGr" style='display:none'>
                <a class="page-link" href="#">Следующая<span aria-hidden="true">→</span></a>
            </li>
        </ul>
    </div>
    <c:if test="${countGroup == 0}">
        <h4>Поиск не дал результатов</h4>
    </c:if>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/ajaxSearchResult.js"></script>
<script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>
</body>
</html>