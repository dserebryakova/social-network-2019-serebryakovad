<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/chat.css">

    <title>Сообщения</title>
</head>

<body>
<div class="container py-5 px-4">
    <!-- For demo purpose-->
    <!-- Chat Box-->
    <div class="col-lg-12 px-0">
        <div class="bg-gray px-4 py-2 bg-light">
            <p class="h5 mb-0 py-1">Сообщения</p>
        </div>
        <div class="px-4 py-5 chat-box bg-white" id="formChat">
            <form role="form">
                <div class="row">
                    <c:if test="${empty lastMessages}">
                        <label style="width: 1000px;" align="center">У вас еще нет сообщений</label>
                    </c:if>
                    <div class="messages-box" align="center">
                        <div class="center-block">
                            <c:forEach items="${lastMessages}" var="lm">
                                <c:set var="messageAccount"
                                       value="${allMessageAccount[lm.recipient]}"/>
                                <a href="/chat?idSender=${lm.sender.id}&idRecipient=${lm.recipient}"
                                   class="list-group-item list-group-item-action list-group-item-light rounded-0"
                                   style="width: 1101px;">
                                    <div class="media">
                                        <c:choose>
                                            <c:when test="${lm.sender.id == sessionScope.user.id}"><socialTags:imgAccountMessage
                                                    account="${messageAccount}"/></c:when>
                                            <c:otherwise><socialTags:imgAccountMessage
                                                    account="${lm.sender}"/></c:otherwise>
                                        </c:choose>
                                        <div class="media-body ml-4">
                                            <div class="d-flex align-items-center justify-content-between mb-1">
                                                <c:choose>
                                                    <c:when test="${lm.sender.id == sessionScope.user.id}">
                                                        <h4 class="mb-0">${messageAccount.name} ${messageAccount.surname}</h4></c:when>
                                                    <c:otherwise><h4
                                                            class="mb-0">${lm.sender.name} ${lm.sender.surname}</h4></c:otherwise>
                                                </c:choose>
                                                <small class="small font-weight-bold">${lm.sendTime}</small>
                                            </div>
                                            <c:if test="${not empty lm.text}">
                                                <p class="text-left font-italic text-muted mb-0 text-small">${lm.text}</p>
                                            </c:if>
                                            <c:if test="${not empty lm.picture}">
                                                <img width="100" height="100" alt="MESS"
                                                     align="left"
                                                     src="${pageContext.servletContext.contextPath }/image?action=message&id=${lm.id}"/>
                                            </c:if>
                                        </div>
                                        <p></p>
                                    </div>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/chat.js"></script>
</body>
</html>
