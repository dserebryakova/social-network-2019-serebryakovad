<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2016/bdt/jquery.bdt.min.css"/>
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2016/bdt/style.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/home.css">

    <title>Страница группы</title>
</head>

<body>

<div class="container">
    <div id="main">

        <div class="row" id="real-estates-detail">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <header class="panel-title">
                            <div class="text-center">
                                <strong>Сообщество</strong>
                            </div>
                        </header>
                    </div>
                    <div class="panel-body">
                        <div class="text-center" id="author">
                            <img width="250" height="250" src="/image?action=group&id=${groupHome.id}" alt="Dafuq!">
                            <h3>${groupHome.name}</h3>
                            <p><font color="#a9a9a9">${groupHome.thematics}</font></p>
                            <p class="sosmed-author">
                                <c:if test="${sessionScope.user.id == groupHome.idOwner}">
                            <form method="get" action="/updateGroupPage">
                                <input type="hidden" name="id" value="${groupHome.id}"/>
                                <button type="submit" value="Submit"
                                        class="btn waves-effect waves-light btn-grd-danger">Панель управления
                                </button>
                            </form>
                            </c:if>
                            <c:if test="${!involvement}">
                                <c:if test="${sessionScope.user.id != owner.id}">
                                    <c:if test="${!checkMemership}">
                                        <form action="/addToGroup" method="post">
                                            <input type="hidden" name="id" value="${groupHome.id}"/>
                                            <button type="submit" value="Submit"
                                                    class="btn waves-effect waves-light btn-grd-warning">
                                                Вступить в группу
                                            </button>
                                        </form>
                                    </c:if>
                                </c:if>
                            </c:if>

                            <c:if test="${involvement}">
                                <p class="label label-warning">Вы участник</p>
                                <p></p>
                                <c:if test="${groupRole=='MODER'}">
                                    <p class="label label-success">МОДЕРАТОР</p>
                                    <p></p>
                                </c:if>
                                <form method="get" action="/deleteFromGroup">
                                    <input type="hidden" name="id" value="${groupHome.id}"/>
                                    <button type="submit" class="btn waves-effect waves-light btn-grd-warning"
                                            name="action" value="delete">Покинуть группу
                                    </button>
                                </form>
                            </c:if>
                            <c:if test="${checkSubc}">
                                <p class="label label-warning">Требуется подтверждение администратора</p>
                                <p></p>
                                <form action="/deleteFromGroup" method="post">
                                    <input type="hidden" name="id" value="${groupHome.id}"/>
                                    <button type="submit" class="btn waves-effect waves-light btn-grd-warning"
                                            name="action" value="delete">Покинуть группу
                                    </button>
                                </form>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <ul id="myTab" class="nav nav-pills">
                            <li class="active"><a href="#detail" data-toggle="tab">О группе</a></li>
                            <li class=""><a href="#contact" data-toggle="tab">Список пользователей</a></li>
                            <c:if test="${sessionScope.user.id == groupHome.idOwner}">
                                <li class=""><a href="#subunk" data-toggle="tab">Запросы</a></li>
                            </c:if>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <hr>

                            <div class="tab-pane fade active in" id="detail">
                                <h4>История группы</h4>
                                <table class="table table-th-block">
                                    <tbody>
                                    <tr>
                                        <td class="active">Дата создания:</td>
                                        <td>${groupHome.regDate}</td>
                                    </tr>
                                    <tr>
                                        <td class="active">Владелец:</td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/home?id=${owner.id}"> ${owner.name} ${owner.surname} </a>
                                        </td>
                                    </tr>
                                    <c:if test="${not empty groupHome.info}">
                                        <tr>
                                            <td class="active">Описание:</td>
                                            <td>${groupHome.info}</td>
                                        </tr>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="contact">
                                <p></p>
                                <table class="table table-th-block" id="bootstrap-table">
                                    <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Имя</th>
                                        <c:if test="${sessionScope.user.id == groupHome.idOwner}">
                                            <th>Роль</th>
                                            <th>Действия</th>
                                        </c:if>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${members}" var="mem">
                                        <tr>
                                            <td>${mem.id}</td>
                                            <td>
                                                <a href="${pageContext.request.contextPath}/home?id=${mem.member.id}">${mem.member.name} ${mem.member.surname}</a>
                                            </td>
                                            <c:if test="${sessionScope.user.id == groupHome.idOwner}">
                                                <td>${mem.groupRole}</td>
                                                <td>
                                                    <c:if test="${mem.groupRole == 'MODER'}">
                                                        <form action="/updateRoleGroup" method="post">
                                                            <input type="hidden" name="action"
                                                                   value="makeMember"/>
                                                            <input type="hidden" name="idRow"
                                                                   value="${mem.id}"/>
                                                            <input type="hidden" name="id"
                                                                   value="${groupHome.id}"/>
                                                            <button type="submit" value="Submit"
                                                                    class="btn-link btn waves-effect waves-light btn-grd-info">
                                                                Сделать пользователем
                                                            </button>
                                                        </form>
                                                    </c:if>
                                                    <c:if test="${mem.groupRole == 'MEMBER'}">
                                                        <form action="/updateRoleGroup" method="post">
                                                            <input type="hidden" name="action" value="makeModer"/>
                                                            <input type="hidden" name="idRow" value="${mem.id}"/>
                                                            <input type="hidden" name="id" value="${groupHome.id}"/>
                                                            <button type="submit" value="Submit"
                                                                    class="btn-link btn waves-effect waves-light btn-grd-info">
                                                                Сделать админом
                                                            </button>
                                                        </form>
                                                    </c:if>
                                                    <p></p>
                                                    <form action="/deleteByAdmin" method="post">
                                                        <input type="hidden" name="idMember"
                                                               value="${mem.member.id}"/>
                                                        <input type="hidden" name="id"
                                                               value="${groupHome.id}"/>
                                                        <button type="submit" value="Submit"
                                                                class="btn-link btn waves-effect waves-light btn-grd-info">
                                                            Удалить
                                                        </button>
                                                    </form>
                                                </td>
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="subunk">
                                <p></p>
                                <table class="table table-th-block" id="bootstrap-table2">
                                    <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Имя</th>
                                        <c:if test="${sessionScope.user.id == groupHome.idOwner}">
                                            <th>Роль</th>
                                            <th>Действия</th>
                                        </c:if>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${subs}" var="sub">
                                        <tr>
                                            <td>${sub.member.id}</td>
                                            <td>
                                                <a href="${pageContext.request.contextPath}/home?id=${sub.member.id}">${sub.member.name} ${sub.member.surname}</a>
                                            </td>
                                            <td>${sub.groupRole}</td>
                                            <td>
                                                <form action="/updateRoleGroup" method="post">
                                                    <input type="hidden" name="action"
                                                           value="makeMember"/>
                                                    <input type="hidden" name="idRow"
                                                           value="${sub.id}"/>
                                                    <input type="hidden" name="id"
                                                           value="${groupHome.id}"/>
                                                    <button type="submit" value="Submit"
                                                            class="btn-link btn waves-effect waves-light btn-grd-info">
                                                        Сделать пользователем
                                                    </button>
                                                    <p></p>
                                                </form>
                                                <form action="/deleteByAdmin" method="post">
                                                    <input type="hidden" name="idMember"
                                                           value="${sub.member.id}"/>
                                                    <input type="hidden" name="id"
                                                           value="${groupHome.id}"/>
                                                    <button type="submit" value="Submit"
                                                            class="btn-link btn waves-effect waves-light btn-grd-info">
                                                        Удалить
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:if test="${groupRole!=null && groupRole!='SUBSCRIBER'}">
    <section class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <form action="/addMessage" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="idRecipient" value="${groupHome.id}"/>
                        <input type="hidden" name="typeMess" value="wallGroup"/>
                        <div class="panel-body">
                            <p class="linkstyle">${nullMessage}</p>
                            <textarea type="text" name="messanger" class="form-control" rows="2"
                                      placeholder="Добавьте Ваш комментарий"></textarea>
                            <input type="file" name="file_uploaded" value="Upload"/>
                            <div class="mar-top clearfix">
                                <button class="btn btn-sm btn-primary pull-right" type="submit"><i
                                        class="fa fa-pencil fa-fw"></i> Добавить
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <div class="media-block">
                            <c:forEach items="${wallGroup}" var="wall">
                                <div class="media-block">
                                    <a class="media-left"><socialTags:infoAboutAccount
                                            account="${wall.sender}"/></a>
                                    <c:if test="${groupRole == 'ADMIN' || groupRole == 'MODER'}">
                                        <a class="media-right">
                                            <form action="/deleteMessage" method="post">
                                                <input type="hidden" name="id" value="${groupHome.id}"/>
                                                <input type="hidden" name="idRow" value="${wall.id}"/>
                                                <input type="hidden" name="typeMess" value="wallGroup"/>
                                                <button type="submit" value="Submit"
                                                        class="btn waves-effect waves-light btn-grd-danger">Удалить
                                                    сообщение
                                                </button>
                                            </form>
                                        </a>
                                    </c:if>
                                    <p></p>
                                    <div class="media-body">
                                        <div class="mar-btm">
                                            <p class="text-muted text-sm"><i
                                                    class="fa fa-mobile fa-lg"></i>${wall.sendTime}</p>
                                        </div>
                                        <c:if test="${not empty wall.text}">
                                            <h2><strong><em>${wall.text}</em></strong></h2>
                                        </c:if>
                                        <c:if test="${not empty wall.picture}">
                                            <socialTags:imgMessage message="${wall}"/>
                                        </c:if>
                                        <hr>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</c:if>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
<script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>
<script src="https://bootstraptema.ru/plugins/2016/bdt/jquery.sortelements.js" type="text/javascript"></script>
<script src="https://bootstraptema.ru/plugins/2016/bdt/jquery.bdt.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/searchInTable.js"></script>

</body>
</html>
