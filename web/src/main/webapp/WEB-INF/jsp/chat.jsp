<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/panel.css">
    <title>Чат</title>
</head>
<body>

<div class="container py-5 px-4">
    <!-- For demo purpose-->
    <!-- Chat Box-->
    <div class="col-lg-12 px-0">
        <div class="bg-gray px-4 py-2 bg-light">
            <p class="h5 mb-0 py-1">Сообщения</p>
        </div>
        <div class="px-4 py-5 chat-box bg-white" id="formChat">
            <!-- Sender Message-->
            <!-- Reciever Message-->
            <c:if test="${not empty messages}">
                <c:forEach items="${messages}" var="mess">
                    <c:choose>
                        <c:when test="${mess.sender.id == sessionScope.user.id}">
                            <div class="media w-50 ml-auto mb-3"><socialTags:imgAccountMessage
                                    account="${sessionScope.user}"/>
                                <div class="media-body">
                                    <div class="d-flex align-items-center justify-content-between mb-1">
                                        <a href="/home?id=${sessionScope.user.id}">
                                            <h4 class="mb-0">${sessionScope.user.name} ${sessionScope.user.surname}</h4>
                                        </a>
                                    </div>
                                    <div class="bg-primary rounded py-2 px-3 mb-2">
                                        <p class="text-small mb-0 text-white">${mess.text}</p>
                                        <p><socialTags:imgMessage message="${mess}"/></p>
                                    </div>
                                    <p class="small text-muted">${mess.sendTime}</p>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <!-- Reciever Message-->
                            <div class="media w-50 mb-3"><socialTags:imgAccountMessage account="${mess.sender}"/>
                                <div class="media-body ml-3">
                                    <div class="d-flex align-items-center justify-content-between mb-1">
                                        <a href="/home?id=${mess.sender.id}">
                                            <h4 class="mb-0">${mess.sender.name} ${mess.sender.surname}</h4>
                                        </a>
                                    </div>
                                    <div class="bg-light rounded py-2 px-3 mb-2">
                                        <p class="text-small mb-0 text-muted">${mess.text}</p>
                                        <socialTags:imgMessage message="${mess}"/>
                                    </div>
                                    <p class="small text-muted">${mess.sendTime}</p>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:if>
            <div id="templateRowMess">
                <div id="selectedMess" class="media w-50 ml-auto mb-3"></div>
            </div>
        </div>
        <!-- Typing area -->
        <div class="panel">
            <div id="idSender" hidden>${sessionScope.user.id}</div>
            <div id="idRecipient" hidden>${idInterlocutor}</div>
            <div class="panel-body">
                    <textarea type="text" id="textMessage" name="messanger" class="form-control" rows="2"
                              placeholder="Добавьте Ваш комментарий"></textarea>
                <div class="mar-top clearfix">
                    <button class="btn btn-sm btn-primary pull-right" style="font-size:24px" type="submit"
                            id="newMessage"><i
                            class="fa fa-paper-plane"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/chat.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/websocket.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.3.0/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

</body>
</html>


