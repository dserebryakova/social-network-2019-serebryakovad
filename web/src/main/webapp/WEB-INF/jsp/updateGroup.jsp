<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>
<jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/registration.css">

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Изменение группы</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<div align="center">

    <h1>Изменение группы ID: ${group.id} Название: ${group.name}</h1>
    <h4>* Обязательные поля</h4>
    <form action="/updateGroup" method="post" enctype="multipart/form-data" modelAttribute="group">
        <table style="with: 80%">
            <input type="hidden" name="id" value="${group.id}"/>
            <input type="hidden" name="regDate" value="${group.regDate}"/>
            <tr>
                <td>Название*</td>
                <td><input type="text" value="${group.name}" name="name"
                           class="w3-input w3-border w3-round-large" required/></td>
            </tr>
            <tr>
                <td>ID Владельца*</td>
                <td><input type="text" value="${group.idOwner}" name="idOwner"
                           class="w3-input w3-border w3-round-large" required/></td>
            </tr>
            <tr>
                <td>Тематика</td>
                <td><input type="text" value="${group.thematics}" name="thematic"
                           class="w3-input w3-border w3-round-large"/></td>
            </tr>
            <tr>
                <td>Описание(максимум 255 символов)</td>
                <td><textarea name="info" style="width: 400px; height: 200px" value="${group.info}"></textarea>
                </td>
            </tr>
            <tr>
                <td>Изменить фото группы</td>
                <td><input type="file" name="file_uploaded" value="Upload"/></td>
            </tr>
        </table>
        <br><br>
        <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom">Сохранить изменения</button>
    </form>
</div>
</body>
</html>
