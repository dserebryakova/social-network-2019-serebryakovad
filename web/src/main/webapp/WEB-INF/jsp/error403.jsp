<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/403.css">
    <title>403 Error</title>
</head>
<body>
<body>
<jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
<div id="content">
    <div class="content-container">
        <div id="header"><h1>Server Error</h1></div>
        <fieldset>
            <h2>403 - Доступ запрещен.</h2>
            <h3>У вас нет прав на осуществление данной операции</h3>
        </fieldset>
    </div>
</div>
</body>
</body>
</html>
