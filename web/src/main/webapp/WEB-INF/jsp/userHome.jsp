<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/home.css">
    <title>Страница пользователя</title>
</head>

<body>
<div class="container">
    <div id="main">
        <div class="row" id="real-estates-detail">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <header class="panel-title">
                            <div class="text-center">
                                <strong>Пользователь сайта</strong>
                            </div>
                        </header>
                    </div>
                    <div class="panel-body">
                        <div class="text-center" id="author">
                            <img width="250" height="250"
                                 src="${pageContext.servletContext.contextPath }/image?action=account&id=${profileAccount.id}"
                                 alt="Dafuq!">
                            <h3>${profileAccount.name} ${profileAccount.patronymic} ${profileAccount.surname}</h3>
                            <p><i>${profileAccount.moodStatus}</i></p>
                            <p class="sosmed-author">
                                <c:if test="${sessionScope.user.id != profileAccount.id}">
                            <form action="/chat" method="get">
                                <input type="hidden" name="idSender" value="${sessionScope.user.id}"/>
                                <input type="hidden" name="idRecipient" value="${profileAccount.id}"/>
                                <button type="submit" class="btn waves-effect waves-light btn-grd-info"><h5>Отправить
                                    сообщение</h5></button>
                            </form>
                            <p></p>
                            <br>
                            <c:if test="${!usersHaveFriendship}">
                                <c:choose>
                                    <c:when test="${!checkSubc}">
                                        <form method="post" action="/updateFriends">
                                            <input type="hidden" name="action" value="addToFriends"/>
                                            <input type="hidden" name="friend" value="${profileAccount.id}"/>
                                            <button type="submit" class="btn waves-effect waves-light btn-grd-warning">
                                                <h3>Добавить в
                                                    друзья</h3></button>
                                        </form>
                                        <br>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="gradient-border">✓ Запрос отправлен</div>
                                        <p></p>
                                        <form method="post" action="/updateFriends">
                                            <input type="hidden" name="action" value="remove"/>
                                            <input type="hidden" name="friend" value="${profileAccount.id}"/>
                                            <button type="submit" value="Submit"
                                                    class="btn waves-effect waves-light btn-grd-danger">Удалить запрос
                                            </button>
                                        </form>
                                        <br>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${usersHaveFriendship}">
                                <p class="label label-warning text-sm">Ваш друг</p>
                                <p></p>
                                <form method="post" action="/updateFriends">
                                    <input type="hidden" name="action" value="remove"/>
                                    <input type="hidden" name="friend" value="${profileAccount.id}"/>
                                    <button type="submit" value="Submit"
                                            class="btn waves-effect waves-light btn-grd-danger">Удалить из друзей
                                    </button>
                                </form>
                                <br>
                            </c:if>
                            </c:if>
                            <c:if test="${sessionScope.user.id == profileAccount.id}">
                                <a href="convertXml?id=${sessionScope.user.id}">
                                    <button type="submit" class="btn waves-effect waves-light btn-grd-warning">
                                        Экспорт данных в XML
                                    </button>
                                </a>
                                <br>
                                <form action="${pageContext.request.contextPath}/edit" method="get">
                                    <input type="hidden" name="id"
                                           value="${sessionScope.user.id}"/>
                                    <button type="submit" class="btn waves-effect waves-light btn-grd-warning">
                                        Редактировать профиль
                                    </button>
                                </form>
                                <p></p>
                                <form action="${pageContext.request.contextPath}/passwordPage" method="get">
                                    <input type="hidden" name="id"
                                           value="${sessionScope.user.id}"/>
                                    <button type="submit" class="btn waves-effect waves-light btn-grd-warning">
                                        Изменить пароль
                                    </button>
                                </form>
                                <p></p>
                                <form action="/lastmess" method="get">
                                    <button type="submit" class="btn waves-effect btn-grd-info">
                                        Сообщения
                                    </button>
                                </form>
                                <p></p>
                                <c:if test="${sessionScope.user.role == 'ROLE_ADMIN'}">
                                    <form action="/admin/panelAdmin" method="get">
                                        <input type="hidden" name="id"
                                               value="${sessionScope.user.id}"/>
                                        <button type="submit" value="Submit"
                                                class="btn waves-effect waves-light btn-grd-danger">Панель управления
                                        </button>
                                    </form>
                                </c:if>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <ul id="myTab" class="nav nav-pills">
                            <li class="active"><a href="#detail" data-toggle="tab">О пользователе</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <hr>
                            <div class="tab-pane fade active in" id="detail">
                                <h4>История профиля</h4>
                                <table class="table table-th-block">
                                    <tbody>
                                    <tr>
                                        <td class="active">Зарегистрирован:</td>
                                        <td>${profileAccount.regDate}</td>
                                    </tr>
                                    <c:if test="${not empty profileAccount.birthdate}">
                                        <tr>
                                            <td class="active">День Рождения:</td>
                                            <td>${profileAccount.birthdate}</td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${not empty homePhones}">
                                        <c:forEach var="phone" items="${homePhones}">
                                            <tr>
                                                <td class="active">Домашний телефон:</td>
                                                <td>${phone.number}</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${not empty workPhones}">
                                        <c:forEach var="phoneWork" items="${workPhones}">
                                            <tr>
                                                <td class="active">Рабочий телефон:</td>
                                                <td>${phoneWork.number}</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${not empty profileAccount.addressHome}">
                                        <tr>
                                            <td class="active">Домашний адрес:</td>
                                            <td>${profileAccount.addressHome}</td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${not empty profileAccount.addressWork}">
                                        <tr>
                                            <td class="active">Рабочий адрес:</td>
                                            <td>${profileAccount.addressWork}</td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${not empty profileAccount.icq}">
                                        <tr>
                                            <td class="active">ICQ:</td>
                                            <td>${profileAccount.icq}</td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${not empty profileAccount.skype}">
                                        <tr>
                                            <td class="active">Skype:</td>
                                            <td>${profileAccount.skype}</td>
                                        </tr>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel">
                <form action="/addMessage" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="idRecipient" value="${profileAccount.id}"/>
                    <input type="hidden" name="typeMess" value="wallAccount"/>
                    <div class="panel-body">
                        <p class="linkstyle">${nullMessage}</p>
                        <textarea type="text" name="messanger" class="form-control" rows="2"
                                  placeholder="Добавьте Ваш комментарий"></textarea>
                        <div class="col">
                            <input type="file" id="uploadImage" name="file_uploaded" align="right"
                                   class="form-control-file" value="Upload"/>
                        </div>
                        <div class="mar-top clearfix">
                            <button class="btn btn-sm btn-primary pull-right" type="submit"><i
                                    class="fa fa-pencil fa-fw"></i> Добавить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel">
                <div class="panel-body">
                    <div class="media-block">
                        <c:forEach items="${wallUser}" var="wall">
                            <div class="media-block">
                                <a class="media-left"><socialTags:infoAboutAccount account="${wall.sender}"/></a>
                                <c:if test="${sessionScope.id == wall.recipient}">
                                    <a class="media-right">
                                        <form action="/deleteMessage" method="post">
                                            <input type="hidden" name="idRow" value="${wall.id}"/>
                                            <input type="hidden" name="typeMess" value="wallAccount"/>
                                            <input type="hidden" name="id" value="${profileAccount.id}"/>
                                            <button type="submit" value="Submit"
                                                    class="btn waves-effect waves-light btn-grd-danger">Удалить
                                                сообщение
                                            </button>
                                        </form>
                                    </a>
                                </c:if>
                                <p></p>
                                <div class="media-body">
                                    <div class="mar-btm">
                                        <p class="text-muted text-sm"><i class="fa fa-mobile fa-lg"></i>${wall.sendTime}
                                        </p>
                                    </div>
                                    <c:if test="${not empty wall.text}">
                                        <h2><strong><em>${wall.text}</em></strong></h2>
                                    </c:if>
                                    <c:if test="${not empty wall.picture}">
                                        <socialTags:imgMessage message="${wall}"/>
                                    </c:if>
                                    <hr>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
<script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>

</body>
</html>
