<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/friendsAndGroups.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Друзья</title>
</head>

<body>
<h2 align="center">Мои друзья</h2>
<p></p>
<div class="friend">
    <c:if test="${not empty friends}">
        <table class="friends-top" cellspacing="15">
            <c:forEach items="${friends}" var="fr">
                <tr>
                    <td>
                        <socialTags:infoAboutAccount account="${fr}"/>
                    </td>
                    <td>
                        <socialTags:buttonFriend account="${fr}" button="danger" action="remove" name="Удалить"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
<c:if test="${empty friends}">
    <h4 align="center">У вас еще нет друзей</h4>
</c:if>
<p></p>
<h2 align="center">Мои подписчики</h2>
<p></p>
<div class="sub">
    <c:if test="${not empty subscription}">
        <table class="friends-top" cellspacing="15">
            <c:forEach items="${subscription}" var="sub">
                <tr>
                    <td>
                        <socialTags:infoAboutAccount account="${sub}"/>
                    </td>
                    <td>
                        <socialTags:buttonFriend account="${sub}" button="danger" action="accept"
                                                 name="Добавить в друзья"/>
                    </td>
                    <td>
                        <socialTags:buttonFriend account="${sub}" button="danger" action="remove"
                                                 name="Отклонить заявку"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
<c:if test="${empty subscription}">
    <h4 align="center">У вас еще нет подписчиков</h4>
</c:if>
<p></p>
<h2 align="center">Мои запросы в друзья</h2>
<p></p>
<div class="subc">
    <c:if test="${not empty subscribers}">
        <table class="friends-top" cellspacing="15">
            <c:forEach items="${subscribers}" var="subsc">
                <tr>
                    <td>
                        <socialTags:infoAboutAccount account="${subsc}"/>
                    </td>
                    <td>
                        <socialTags:buttonFriend account="${subsc}" button="danger" action="remove" name="Удалить"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
<c:if test="${empty subscribers}">
    <h4 align="center">Нет запросов</h4>
</c:if>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
</body>
</html>
