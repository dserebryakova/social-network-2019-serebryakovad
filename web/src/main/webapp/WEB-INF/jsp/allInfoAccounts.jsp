<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2016/bdt/style.css"/>
    <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2016/bdt/jquery.bdt.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/accounts.css">
    <title>Панель управление пользователями</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="box clearfix">
            <h2 class="text-center">Панель управления пользователями</h2>
            <hr/>
            <table class="table table-hover" id="bootstrap-table">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Пользователь</th>
                    <th>Второе имя</th>
                    <th>Email</th>
                    <th>Дата регистрации</th>
                    <th>Роль</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${accounts}" var="account">
                    <tr>
                        <td> ${account.id} </td>
                        <td width="200"><socialTags:infoAboutAccountForAdmin account="${account}"/></td>
                        <td> ${account.patronymic} </td>
                        <td> ${account.email} </td>
                        <td> ${account.regDate} </td>
                        <c:if test="${account.role == 'ROLE_ADMIN'}">
                            <td>Администратор</td>
                        </c:if>
                        <c:if test="${account.role == 'ROLE_USER'}">
                            <td>Пользователь</td>
                        </c:if>
                        <td>
                            <form action="/edit" method="get">
                                <input type="hidden" name="id"
                                       value="${account.id}"/>
                                <button type="submit" value="Submit"
                                        class="btn-link btn waves-effect waves-light btn-grd-info">
                                    Изменить
                                </button>
                            </form>
                            <form action="/admin/delete" method="get">
                                <input type="hidden" name="id"
                                       value="${account.id}"/>
                                <button type="submit" value="Submit"
                                        class="btn-link btn waves-effect waves-light btn-grd-danger">
                                    Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>
<script src="https://bootstraptema.ru/plugins/2016/bdt/jquery.sortelements.js" type="text/javascript"></script>
<script src="https://bootstraptema.ru/plugins/2016/bdt/jquery.bdt.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/searchInTable.js"></script>
</body>
</html>
