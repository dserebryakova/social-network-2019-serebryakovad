<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/edit.css">

    <title>Reg Success</title>
</head>
<body>
<div class="w3-container w3-content w3-display-middle">
    <div class="w3-col m12">
        <div class="w3-card w3-round w3-white">
            <div class="w3-container w3-padding">
                <div class="w3-container w3-center">
                    <p><label>Регистрация</label>
                    <p>
                    <hr>
                    <p>Поздравляем! Вы зарегестрированы. Войдите используя имя пользователя и пароль.
                    <p>
                        <a href="index.jsp" class="w3-button w3-theme">На страницу входа</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
