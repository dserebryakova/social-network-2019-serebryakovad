<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/registration.css">

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Регистрация</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="header">
    <h1>Soc***Net</h1>
</div>

<body>
<div align="center">
    <h1>Регистрация нового пользователя</h1>
    <h4>* Обязательные поля</h4>
    <form action="/checkRegister" method="post" modelAttribute="newAccount">
        <table style="with: 80%">
            <tr>
                <td>Имя*</td>
                <td><input type="text" name="name" class="w3-input w3-border w3-round-large" required/></td>
            </tr>
            <tr>
                <td>Фамилия</td>
                <td><input type="text" name="surname" class="w3-input w3-border w3-round-large"/></td>
            </tr>
            <c:if test="${error!=null}">
                <p class="w3-text-red">Данный email уже используется</p>
            </c:if>
            <tr>
                <td>Email*</td>
                <td><input type="email" name="email" class="w3-input w3-border w3-round-large" required/></td>
            </tr>
            <tr>
                <td>Пароль*</td>
                <td><input type="text" name="password" class="w3-input w3-border w3-round-large" required/></td>
            </tr>
        </table>
        <br><br>
        <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom">Зарегистрироваться</button>
    </form>
</div>
<div class="w3-container w3-grey w3-opacity w3-right-align w3-padding">
    <button class="w3-btn w3-round-large" onclick="location.href='/index.jsp'">Back to main</button>
</div>
</body>
</html>
