<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <meta charset="UTF-8">
    <title>Изменение пароля</title>

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:300,400'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/changePassword.css">

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
            <form action="${pageContext.request.contextPath}/updatePassword" class="form-horizontal mar-top-bot-50"
                  id="validateForm" modelAttribute="changePasswordDto" method="post">
                <h1>Изменение пароля</h1>
                <fieldset>
                    <c:if test="${passwordMessage == 'success'}">
                        <div class="alert alert-success" role="alert">
                            Пароль успешно обновлен
                        </div>
                    </c:if>
                    <c:if test="${passwordMessage == 'error'}">
                        <div class="alert alert-danger" role="alert">
                            Введенный старый пароль не совпадает с текущим
                        </div>
                    </c:if>
                    <!-- Old password input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label">Старый пароль</label>
                        <div class="col-md-12">
                            <input id="oldPassword" name="oldPassword" type="password" placeholder=""
                                   class="form-control input-md" required>
                        </div>
                    </div>
                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label">Новый пароль <span id="popover-password-top"
                                                                                  class="hide pull-right block-help"><i
                                class="fa fa-info-circle text-danger" aria-hidden="true"></i> Введите более длинный пароль</span></label>
                        <div class="col-md-12">
                            <input id="password" name="password" type="password" placeholder=""
                                   class="form-control input-md" data-placement="bottom" data-toggle="popover"
                                   data-container="body" type="button" data-html="true" required>
                            <div id="popover-password">
                                <ul class="list-unstyled">
                                    <li class=""><span class="eight-character"><i class="fa fa-times"
                                                                                  aria-hidden="true"></i></span>&nbsp;
                                        Не менее 4 символов
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Repeat Password -->
                    <div class="form-group">
                        <label class="col-md-12 control-label">Повторите пароль<span id="popover-cpassword"
                                                                                     class="hide pull-right block-help"><i
                                class="fa fa-info-circle text-danger" aria-hidden="true"></i> Пароли не совпадают</span></label>
                        <div class="col-md-12">
                            <input id="confirmPassword" name="confirmPassword" type="password" placeholder=""
                                   class="form-control input-md" required>
                        </div>
                    </div>
                </fieldset>
                <div class="d-flex justify-content-center" align="center">
                    <input type="hidden" name="action" value="update">
                    <button class="btn btn-primary btn-lg" value="ok" type="submit">Сохранить изменения</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.validate.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
<script src="${pageContext.request.contextPath}/resources/js/changePassword.js"></script>

</body>
</html>
