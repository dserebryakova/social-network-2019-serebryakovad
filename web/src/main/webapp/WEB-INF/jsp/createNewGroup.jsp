<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>
<jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/registration.css">

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Создание новой группы</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<div align="center">
    <h1>Создание новой группы</h1>
    <h4>* Обязательные поля</h4>
    <form action="/createGroup" method="post" modelAttribute="group">
        <table style="with: 80%">
            <tr>
                <td>Название группы*</td>
                <td><input type="text" name="name" class="w3-input w3-border w3-round-large" required/></td>
            </tr>
            <tr>
                <td>Тематика</td>
                <td><input type="text" name="thematic" class="w3-input w3-border w3-round-large"/></td>
            </tr>
            <tr>
                <td>Информация о группе</td>
                <td><textarea name='info' style="width: 400px; height: 200px"></textarea></td>
            </tr>
            <tr>
        </table>
        <br><br>
        <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom" name="Создать"
                value="Создать">Создать группу
        </button>
    </form>
</div>
</body>
</html>
