<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/index.css">

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta charset="utf-8">
    <title>Admin Login Form</title>
</head>
<body>
<div class="loginBox">
    <img src="${pageContext.request.contextPath}/resources/picture/admin.png" class="user">
    <h2>SocNet</h2>
    <form action="/login" method="post">
        <c:if test="${error}">
            <p><h5>Неверное имя пользователя или пароль</h5></p>
        </c:if>
        <p>Email</p>
        <input type="text" id="username" name="username" placeholder="Email" required>
        <p>Пароль</p>
        <input type="password" name="password" id="password" placeholder="Password" required>
        <div style="text-align: center"><label><input class="w3-check" name="remember-me" type="checkbox"
                                                      checked="checked"><small>Запомнить меня</small></label></div>

        <input type="submit" value="Вход">
        <a href='/register'>
            <div style="text-align: center;">У вас нет аккаунта? Регистрация</div>
        </a>
    </form>
</div>
</body>
</html>
