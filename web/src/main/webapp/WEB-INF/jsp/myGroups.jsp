<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/friendsAndGroups.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Группы</title>
</head>

<body>
<div class="myGroups">
    <h2 align="center">Управление группами</h2>
    <div class="adminGroup">
        <c:if test="${not empty myGroupsAdmin}">
            <table class="friends-top" cellspacing="15">
                <c:forEach items="${myGroupsAdmin}" var="gr">
                    <tr>
                        <td>
                            <socialTags:infoAboutGroup group="${gr}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <div align="center" class="buttonG">
            <button class="w3-btn w3-green w3-round-large w3-margin-bottom"
                    onclick="location.href='/newGroupPage'">Создать группу
            </button>
        </div>
    </div>
</div>

<div class="subcGroup">
    <h2 align="center">Мои группы</h2>

    <div class="member">
        <c:if test="${not empty myMembership}">
            <table class="friends-top" cellspacing="15">
                <c:forEach items="${myMembership}" var="mem">
                    <tr>
                        <td>
                            <socialTags:infoAboutGroup group="${mem}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </div>
    <c:if test="${empty myMembership}">
        <h4 align="center">Вы не состоите ни в одной группе</h4>
    </c:if>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
</body>
</html>
