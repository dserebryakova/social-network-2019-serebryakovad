<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
</head>
<body>
<ul class="menu-main">
    <li class="left-item current"><a>Soc *** Net</a></li>
    <li class="left-item current"><a href="/home?id=${sessionScope.user.id}">Пользователь: ${sessionScope.user.name}</a>
    </li>
    <li class="right-item"><a href="/logout" method="get">Выйти</a></li>
    <li class="right-item">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="form-inline ml-auto" action="/search">
                <div class="md-form my-0">
                    <input id="searchFilter" class="form-control" type="text" name="search" placeholder="Поиск"
                           aria-label="Search">
                    <button class="btn btn-outline-white btn-md my-0 ml-sm-2" type="submit">Поиск</button>
                </div>
            </form>
        </div>
    </li>
    <li class="right-item">
        <a href="/groups">Группы</a>
    </li>
    <li class="right-item"><a href="/friends">Друзья</a></li>
    <li class="right-item"><a href="/">Главная</a></li>
</ul>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/navbar.css">
</body>
</html>
