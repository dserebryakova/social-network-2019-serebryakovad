<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/jsp/navbar.jsp"/>
<%@taglib prefix="socialTags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/edit.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Обновление аккаунта</title>
</head>
<body>
<div class="container">
    <div class="plane">
        <br>
        <h3 align="center">Изменение пользователя ${account.name} ${account.surname}</h3>
        <hr>
        <br>
        <form action="${pageContext.request.contextPath}/updateFromXml" id="xmlForm" class="form-horizontal form-back"
              method="post" enctype="multipart/form-data">
            <label>Загрузка XML файла для изменения аккаунта</label>
            <div class="form-group">
                <div class="col-sm-10">
                    <input type="file" id="input_xml" name="file_xml"/>
                </div>
            </div>
            <label id="warning"></label>
            <div class="d-flex justify-content-center">
                <input type="hidden" name="id" value="${account.id}">
                <button class="btn btn-success" value="ok" type="submit">Импорт файла XML</button>
            </div>
        </form>
        <hr>
        <form action="${pageContext.request.contextPath}/editDone" id="updateForm" class="form-horizontal form-back"
              method="post" enctype="multipart/form-data" modelAttribute="account">
            <input type="hidden" name="regDate" value="${account.regDate}"/>
            <input type="hidden" name="id" value="${account.id}"/>
            <input type="hidden" name="password" value="${account.password}">
            <img width="250" height="250"
                 src="${pageContext.servletContext.contextPath }/image?action=account&id=${account.id}"
                 alt="Dafuq!">
            <div class="form-group">
                <label class="col-sm-2 control-label">Изменить фото</label>
                <div class="col-sm-10">
                    <input type="file" name="file_uploaded"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Имя*</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.name}" name="name" class="form-control" required>
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.surname}" name="surname" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Второе имя</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.patronymic}" name="patronymic" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email*</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.email}" name="email" class="form-control" required>
                </div>
            </div>
            <br>
            <c:if test="${sessionScope.user.role == 'ROLE_ADMIN'}">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Роль</label>
                    <div class="col-sm-10">
                        <select class="form-control custom-select" name="role">
                            <c:if test="${account.role == 'ROLE_ADMIN'}">
                                <option value="ROLE_ADMIN" selected>Администратор</option>
                                <option value="ROLE_USER">Пользователь</option>
                            </c:if>
                            <c:if test="${account.role == 'ROLE_USER'}">
                                <option value="ROLE_ADMIN">Администратор</option>
                                <option value="ROLE_USER" selected>Пользователь</option>
                            </c:if>
                        </select>
                    </div>
                </div>
            </c:if>
            <br>
            <c:if test="${sessionScope.user.role == 'ROLE_USER'}">
                <input type="hidden" name="role" value="${sessionScope.user.role} "/>
            </c:if>
            <div class="form-group">
                <label class="col-sm-2 control-label">День рождения</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.birthdate}" id="datepicker" name="birthdate"
                           class="form-control" readonly="readonly" style="background:white;">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Домашний адрес</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.addressHome}" name="addressHome" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Рабочий адрес</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.addressWork}" name="addressWork" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">ICQ</label>
                <div class="col-sm-10">
                    <input type="number" value="${account.icq}" name="icq" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Skype</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.skype}" name="skype" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Настрой</label>
                <div class="col-sm-10">
                    <input type="text" value="${account.moodStatus}" name="moodStatus" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-2 control-label">Телефоны</label>
                <div class="col-sm-10">
                    <div class="phones">
                        <c:set var="i" value="20"></c:set>
                        <c:forEach var="phone" items="${homePhones}">
                            <div class="input-group phone-input">
                                <div class="input-group-btn">
                                    <select class="btn btn-default dropdown-toggle" name="phones[${i}].phoneType"
                                            aria-expanded="false">
                                        <option value="HOME" selected>Домашний</option>
                                        <option value="WORK">Рабочий</option>
                                    </select>
                                </div>
                                <input type="text" name="phones[${i}].number" class="mask-phone form-control"
                                       value="${phone.number}"/>
                                <span class="input-group-btn">
                                <button class="btn btn-danger btn-remove-phone m8" type="button"><span
                                        class="glyphicon glyphicon-minus"></span></button>
                                </span>
                            </div>
                            <c:set var="i" value="${i + 1}"></c:set>
                        </c:forEach>
                        <c:set var="i" value="40"></c:set>
                        <c:forEach var="phone" items="${workPhones}">
                            <div class="input-group phone-input">
                                <div class="input-group-btn">
                                    <select class="btn btn-default dropdown-toggle" name="phones[${i}].phoneType"
                                            aria-expanded="false">
                                        <option value="WORK" selected>Рабочий</option>
                                        <option value="HOME">Домашний</option>
                                    </select>
                                </div>
                                <input type="text" name="phones[${i}].number" class="mask-phone form-control"
                                       value="${phone.number}"/>
                                <span class="input-group-btn">
                                <button class="btn btn-danger btn-remove-phone m8" type="button">
                                    <span class="glyphicon glyphicon-minus"></span></button>
                                </span>
                            </div>
                            <c:set var="i" value="${i + 1}"></c:set>
                        </c:forEach>
                    </div>
                </div>
                <button type="button" class="mask-phone btn btn-success btn-sm btn-add-phone m6" id="newPhoneBtn"><span
                        class="glyphicon glyphicon-plus"></span> Add Phone
                </button>
            </div>
            <hr>
            <div class="d-flex justify-content-center" align="center">
                <input type="hidden" name="action" value="update">
                <button class="btn btn-primary btn-lg" value="ok" type="submit">Сохранить изменения</button>
            </div>
        </form>
        <hr>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/updateUser.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>

</body>
</html>
