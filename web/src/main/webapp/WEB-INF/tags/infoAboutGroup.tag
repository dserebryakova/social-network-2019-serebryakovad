<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="group" required="true" type="com.getjavajob.common.Group" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/home.css">

<a href="/homeGroup?id=${group.id}">
    <img width="80" height="80" class="round"
         src="${pageContext.servletContext.contextPath }/image?action=group&id=${group.id}"/><b>${group.name}</b>
</a>