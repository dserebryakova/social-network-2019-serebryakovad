<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="account" required="true" type="com.getjavajob.common.Account" %>
<link rel="stylesheet" href="../../resources/css/home.css">

<a href="/home?id=${account.id}">
    <img width="40" height="40" class="round"
         src="${pageContext.servletContext.contextPath }/image?action=account&id=${account.id}"/>
    ${account.name} ${account.surname}
</a>