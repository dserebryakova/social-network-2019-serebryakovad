<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="group" required="true" type="com.getjavajob.common.Group" %>
<%@ attribute name="button" required="true" %>
<%@ attribute name="action" required="true" %>
<%@ attribute name="name" required="true" %>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<form method="post" action="/group">
    <input type="hidden" name="gr" value="${group.id}"/>
    <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom" name="action"
            value="${action}">${name}</button>
</form>