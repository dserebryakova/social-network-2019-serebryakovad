package com.getjavajob.web;

import com.getjavajob.dao.config.DaoConfig;
import com.getjavajob.service.config.ServiceConfig;
import com.getjavajob.web.config.JmsConfig;
import com.getjavajob.web.config.SecurityConfig;
import com.getjavajob.web.config.WebConfig;
import com.getjavajob.web.config.WebSocketConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value = {
        JmsConfig.class,
        WebConfig.class,
        SecurityConfig.class,
        WebSocketConfig.class,
        ServiceConfig.class,
        DaoConfig.class})
public class SocNet extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(SocNet.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SocNet.class);
    }
}
