package com.getjavajob.web.controller;

import com.getjavajob.common.enums.Role;
import com.getjavajob.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    @Autowired
    public AdminController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/panelAdmin")
    public ModelAndView showPanelAdmin(@SessionAttribute("id") Long idSession,
                                       @SessionAttribute("role") Role roleSession,
                                       @RequestParam("id") Long id) {
        if (!id.equals(idSession) && !roleSession.equals(Role.ROLE_ADMIN)) {
            logger.warn("Attempt to log in to the admin Panel. User: " + idSession);
            return new ModelAndView("error403");
        } else {
            ModelAndView modelAndView = new ModelAndView("allInfoAccounts");
            modelAndView.addObject("accounts", accountService.getAllAccounts());
            return modelAndView;
        }
    }

    @GetMapping("/delete")
    public ModelAndView deleteUserProfile(@SessionAttribute("id") Long idSession,
                                          @SessionAttribute("role") Role roleSession,
                                          @RequestParam("id") Long id) {
        if (!id.equals(idSession) && !roleSession.equals(Role.ROLE_ADMIN)) {
            logger.warn("Attempt to log in to the admin Panel. User: " + idSession);
            return new ModelAndView("error403");
        } else {
            ModelAndView modelAndView = new ModelAndView("allInfoAccounts");
            accountService.deleteAccount(id);
            modelAndView.addObject("accounts", accountService.getAllAccounts());
            logger.info("In deleteUserProfile method, id = " + id);
            return modelAndView;
        }
    }
}
