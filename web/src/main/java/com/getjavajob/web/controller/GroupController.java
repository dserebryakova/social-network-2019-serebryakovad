package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.common.Group;
import com.getjavajob.common.GroupMember;
import com.getjavajob.common.Message;
import com.getjavajob.common.enums.GroupRole;
import com.getjavajob.common.enums.MessageType;
import com.getjavajob.service.GroupService;
import com.getjavajob.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Controller
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    private final GroupService groupService;
    private final MessageService messageService;

    @Autowired
    public GroupController(GroupService groupService, MessageService messageService) {
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @GetMapping("/groups")
    public ModelAndView showGroups(@SessionAttribute("id") Long idSession) {
        List<Group> myGroupsAdmin = groupService.getByIdOwner(idSession);
        List<Group> myMembership = groupService.getMembership(idSession);
        ModelAndView modelAndView = new ModelAndView("myGroups");
        modelAndView.addObject("myGroupsAdmin", myGroupsAdmin);
        modelAndView.addObject("myMembership", myMembership);
        return modelAndView;
    }

    @GetMapping("/homeGroup")
    public ModelAndView showHomeGroup(@RequestParam("id") Long idGroup, @SessionAttribute("id") Long idSession) {
        logger.info("In homeGroup method, id = " + idSession);
        Group group = groupService.getById(idGroup);
        List<GroupMember> members = groupService.getAllMembers(idGroup);
        List<GroupMember> subs = groupService.getAllSubs(idGroup);
        List<GroupMember> allMembers = groupService.getAllMembersFromGrMem(idGroup);
        List<Message> wallGroup = messageService.getMessageForWall(idGroup, MessageType.GROUP_WALL);
        Account owner = groupService.getInfoOwner(idGroup, group.getIdOwner());
        GroupRole groupRole = groupService.getRole(idGroup, idSession);
        if (groupRole == null) {
            if (idSession.equals(owner.getId())) {
                groupRole = GroupRole.ADMIN;
            }
            logger.info("In homeGroup method, Admin role id = " + idSession);
        }
        boolean involvement = false;
        boolean checkSubc = false;

        for (GroupMember membership : members) {
            if (idSession.equals(membership.getMember().getId())) {
                involvement = true;
                break;
            }
        }

        for (GroupMember memberSubs : subs) {
            if (idSession.equals(memberSubs.getMember().getId())) {
                checkSubc = true;
                break;
            }
        }

        boolean checkMemership = groupService.checkMembership(idGroup, idSession);
        ModelAndView modelAndView = new ModelAndView("groupHome");
        modelAndView.addObject("groupRole", groupRole);
        modelAndView.addObject("wallGroup", wallGroup);
        modelAndView.addObject("allMembers", allMembers);
        modelAndView.addObject("checkSubc", checkSubc);
        modelAndView.addObject("checkMemership", checkMemership);
        modelAndView.addObject("involvement", involvement);
        modelAndView.addObject("owner", owner);
        modelAndView.addObject("subs", subs);
        modelAndView.addObject("groupHome", group);
        modelAndView.addObject("members", members);
        return modelAndView;
    }

    @GetMapping("/newGroupPage")
    public String showCreateGroup() {
        return "createNewGroup";
    }

    @PostMapping("/createGroup")
    public String createGroup(@ModelAttribute Group group, @SessionAttribute("id") Long idSession) {
        logger.info("In createGroup method, id = " + idSession);
        String regDate = LocalDate.now(ZoneId.of("Europe/Paris")).toString();
        group.setRegDate(regDate);
        group.setIdOwner(idSession);

        groupService.createGroup(group);
        return "redirect:/groups";
    }

    @GetMapping("/updateGroupPage")
    public ModelAndView showUpdateGroup(@RequestParam("id") Long idGroup, @SessionAttribute("id") Long idSession) {
        logger.info("In showUpdateGroup method, id = " + idSession);
        Group group = groupService.getById(idGroup);
        if (!idSession.equals(group.getIdOwner())) {
            logger.warn("Attempt to log in to the admin Panel Group. User: " + idSession);
            return new ModelAndView("error403");
        } else {
            ModelAndView modelAndView = new ModelAndView("updateGroup");
            modelAndView.addObject("group", group);
            return modelAndView;
        }
    }

    @PostMapping("/updateGroup")
    public String updateGroup(@ModelAttribute Group group,
                              @RequestParam(required = false, name = "file_uploaded") MultipartFile photo,
                              @SessionAttribute("id") Long idSession) throws IOException {
        logger.info("In updateGroup method, id = " + idSession);
        if (!idSession.equals(group.getIdOwner())) {
            group.setIdOwner(group.getIdOwner());
        }
        if (photo.isEmpty()) {
            group.setPhoto(groupService.getOldPhoto(group.getId()));
        } else {
            group.setPhoto(photo.getBytes());
        }
        groupService.updateGroup(group);
        return "redirect:/groups";
    }

    @PostMapping("/updateRoleGroup")
    public String updateRoleGroup(@RequestParam(required = false, name = "id") Long idGroup,
                                  @RequestParam(required = false, name = "action") String action,
                                  @RequestParam(required = false, name = "idRow") Long idRow) {
        logger.info("In updateRoleGroup method, idGroup = " + idGroup);
        switch (action) {
            case "makeMember":
                groupService.updateMember(idRow);
                break;
            case "makeModer":
                groupService.updateModer(idRow);
                break;
        }
        return "redirect:/homeGroup?id=" + idGroup;
    }

    @PostMapping("/addToGroup")
    public String addToGroup(@RequestParam(required = false, name = "id") Long idGroup,
                             @SessionAttribute("user") Account account) {
        logger.info("In addToGroup method, idAccount = " + account.getId() + ", idGroup = " + idGroup);
        groupService.insertMember(idGroup, account.getId());
        return "redirect:/groups";
    }

    @PostMapping("/deleteByAdmin")
    public String deleteByAdmin(@RequestParam(required = false, name = "id") Long idGroup,
                                @RequestParam(required = false, name = "idMember") Long idMember) {
        logger.info("In deleteByAdmin method, idMember = " + idMember + ", idGroup = " + idGroup);
        groupService.deleteMember(idGroup, idMember);
        return "redirect:/homeGroup?id=" + idGroup;
    }

    @PostMapping("/deleteFromGroup")
    public String deleteFromGroup(@RequestParam(required = false, name = "id") Long idGroup,
                                  @SessionAttribute("user") Account account) {
        logger.info("In deleteFromGroup method, idMember = " + account.getId() + ", idGroup = " + idGroup);
        groupService.deleteMember(idGroup, account.getId());
        return "redirect:/groups";
    }
}
