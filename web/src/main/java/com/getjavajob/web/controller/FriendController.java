package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.common.Friend;
import com.getjavajob.service.AccountService;
import com.getjavajob.service.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class FriendController {
    private static final Logger logger = LoggerFactory.getLogger(FriendController.class);

    private final AccountService accountService;
    private final FriendService friendService;

    @Autowired
    public FriendController(AccountService accountService, FriendService friendService) {
        this.accountService = accountService;
        this.friendService = friendService;
    }

    @GetMapping("/friends")
    public ModelAndView showFriends(@SessionAttribute("id") long idSession) {
        logger.info("In showFriends method, id = " + idSession);
        List<Account> friends = friendService.getFriends(idSession);
        List<Account> subscription = friendService.getSubscriptions(idSession);
        List<Account> subscribers = friendService.getReqFriends(idSession);
        ModelAndView modelAndView = new ModelAndView("myFriends");
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("subscription", subscription);
        modelAndView.addObject("subscribers", subscribers);
        return modelAndView;
    }

    @PostMapping("/updateFriends")
    public String addFriend(@RequestParam("friend") long idFriend, @RequestParam("action") String action,
                            @SessionAttribute("id") long idSession) {
        logger.info("In addFriend method, id = " + idSession);
        Friend friend = new Friend();
        Friend approveFriend = new Friend();
        Account accountMaster = accountService.getById(idSession);
        Account accountFriend = accountService.getById(idFriend);
        friend.setAccountMaster(accountMaster);
        friend.setAccountFriend(accountFriend);
        approveFriend.setAccountMaster(accountFriend);
        approveFriend.setAccountFriend(accountMaster);
        switch (action) {
            case "addToFriends":
                friendService.requestFriend(friend);
                break;
            case "accept":
                friendService.approveFriend(approveFriend);
                break;
            case "remove":
                friendService.deleteFriend(friend);
                break;
        }
        return "redirect:/friends";
    }
}
