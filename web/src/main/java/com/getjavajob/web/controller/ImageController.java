package com.getjavajob.web.controller;

import com.getjavajob.service.AccountService;
import com.getjavajob.service.GroupService;
import com.getjavajob.service.MessageService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class ImageController {
    private final AccountService accountService;
    private final GroupService groupService;
    private final MessageService messageService;

    @Autowired
    public ImageController(AccountService accountService, GroupService groupService, MessageService messageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @GetMapping("/image")
    @ResponseBody
    public byte[] getImage(String action, long id, HttpServletRequest request) throws IOException {
        byte[] image = null;
        String defaultImage = null;
        switch (action) {
            case "account":
                image = accountService.getPhoto(id);
                defaultImage = "/resources/picture/an.jpeg";
                break;
            case "group":
                image = groupService.getPhoto(id);
                defaultImage = "/resources/picture/group.jpg";
                break;
            case "message":
                image = messageService.getPhoto(id);
                break;
        }
        if (image == null) {
            image = IOUtils.toByteArray(request.getServletContext().getResourceAsStream(defaultImage));
        }
        return image;
    }
}
