package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    private final AccountService accountService;

    @Autowired
    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping({"/", "/loginForm"})
    public String showLoginPage(@RequestParam(value = "error", required = false) String error,
                                Model model, Authentication authentication, HttpSession session) {

        if (authentication != null) {
            Long userIdInSession = (Long) session.getAttribute("id");
            if (userIdInSession == null) {
                Account account = accountService.getAccountByEmail(authentication.getName());
                userIdInSession = account.getId();
                session.setAttribute("id", userIdInSession);
                session.setAttribute("user", account);
                session.setAttribute("role", account.getRole());

            }
            return "redirect:/home?id=" + userIdInSession;
        }
        model.addAttribute("error", error != null);
        return "index";
    }

    @GetMapping("/accessError")
    public String errorAccessPage() {
        return "error403";
    }
}
