package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.common.Message;
import com.getjavajob.common.Phone;
import com.getjavajob.common.dto.ChangePasswordDto;
import com.getjavajob.common.enums.MessageType;
import com.getjavajob.common.enums.Role;
import com.getjavajob.service.AccountService;
import com.getjavajob.service.FriendService;
import com.getjavajob.service.MessageService;
import com.getjavajob.web.util.WebUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.web.util.WebUtil.constructPhones;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;
    private final FriendService friendService;
    private final MessageService messageService;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AccountController(AccountService accountService, FriendService friendService, MessageService messageService, BCryptPasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.friendService = friendService;
        this.messageService = messageService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/home")
    public ModelAndView viewAccount(@SessionAttribute("id") Long idSession,
                                    @RequestParam(required = false, name = "id") Long idAccount) {
        logger.info("In viewAccount method, id = " + idAccount);
        boolean usersHaveFriendship = false;
        boolean checkSubc = false;
        long idProfileAccount;
        if (!idAccount.equals(idSession)) {
            idProfileAccount = idAccount;
            usersHaveFriendship = friendService.checkUsersHaveFriendShip(idSession, idProfileAccount, true);
            checkSubc = friendService.checkUsersHaveFriendShip(idSession, idProfileAccount, false);
        } else {
            idProfileAccount = idSession;
        }
        Account profileAccount = accountService.getById(idProfileAccount);
        List<Message> wallUser = messageService.getMessageForWall(idProfileAccount, MessageType.ACCOUNT_WALL);
        List<Phone> phones = profileAccount.getPhones();
        List<Phone> homePhones = new ArrayList<>();
        List<Phone> workPhones = new ArrayList<>();
        WebUtil.getPhones(phones, homePhones, workPhones);
        ModelAndView modelAndView = new ModelAndView("userHome");
        modelAndView.addObject("profileAccount", profileAccount);
        modelAndView.addObject("wallUser", wallUser);
        modelAndView.addObject("usersHaveFriendship", usersHaveFriendship);
        modelAndView.addObject("checkSubc", checkSubc);
        modelAndView.addObject("homePhones", homePhones);
        modelAndView.addObject("workPhones", workPhones);
        return modelAndView;
    }

    @GetMapping("/register")
    public String showRegistrationPage() {
        return "registration";
    }

    @PostMapping("/checkRegister")
    public String checkRegistration(@ModelAttribute("newAccount") Account account, Model model,
                                    @RequestParam(value = "error", required = false) String error) {
        if (accountService.checkEmail(account.getEmail())) {
            model.addAttribute("error", error != null);
            return "registration";
        } else {
            String regDate = LocalDate.now(ZoneId.of("Europe/Paris")).toString();
            account.setRegDate(regDate);
            account.setRole(Role.ROLE_USER);
            account.setPassword(passwordEncoder.encode(account.getPassword()));

            accountService.createAccount(account);
            logger.info("In checkRegistration method, id = " + account.getId());
            return "registrationSuccess";
        }
    }

    @GetMapping("/edit")
    public ModelAndView editUserPage(@SessionAttribute("id") Long idSession,
                                     @SessionAttribute("role") Role roleSession,
                                     @RequestParam("id") Long id) {
        logger.info("In editUserPage method, id = " + idSession);
        if (!id.equals(idSession) && !roleSession.equals(Role.ROLE_ADMIN)) {
            logger.warn("Attempt to log in to the admin Panel. User: " + idSession);
            return new ModelAndView("error403");
        } else {
            ModelAndView modelAndView = new ModelAndView("updateUser");
            Account account = accountService.getById(id);
            List<Phone> phones = account.getPhones();
            List<Phone> homePhones = new ArrayList<>();
            List<Phone> workPhones = new ArrayList<>();
            WebUtil.getPhones(phones, homePhones, workPhones);
            modelAndView.addObject("account", account);
            modelAndView.addObject("homePhones", homePhones);
            modelAndView.addObject("workPhones", workPhones);

            return modelAndView;
        }
    }

    @PostMapping("/editDone")
    public ModelAndView editUserDone(@RequestParam(required = false, name = "file_uploaded") MultipartFile photo,
                                     @ModelAttribute("account") Account account, @SessionAttribute("user") Account sessionAccount,
                                     HttpSession session) throws IOException {
        ModelAndView modelAndView = new ModelAndView("updateSuccess");
        if (photo.isEmpty()) {
            account.setPhoto(accountService.getPhoto(account.getId()));
        } else {
            account.setPhoto(photo.getBytes());
        }

        List<Phone> phones = account.getPhones();
        if (phones != null) {
            account.setPhones(constructPhones(account));
        }
        accountService.updateAccount(account);
        if (sessionAccount.getId().equals(account.getId())) {
            session.removeAttribute("user");
            session.setAttribute("user", account);
        }
        logger.info("In editUserDone method, id = " + account.getId());
        return modelAndView;
    }

    @GetMapping("/passwordPage")
    public String showPasswordUpdatePage() {
        return "changePassword";
    }

    @GetMapping("/convertXml")
    public void convertAccountToXml(@RequestParam("id") Long id, HttpServletResponse response) {
        logger.info("In accountToXml method");
        Account account = accountService.getById(id);
        File file = new File("account.xml");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Account.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(account, file);
        } catch (JAXBException e) {
            logger.error("JAXBException: ", e);
        }
        try (InputStream inputStream = new FileInputStream(file)) {
            response.setContentType("application/xml");
            response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException: ", e);
        } catch (IOException e) {
            logger.error("IOException: ", e);
        }
    }

    @PostMapping("/updateFromXml")
    public ModelAndView updateAccountFromXml(@RequestParam("id") Long id, @RequestParam("file_xml") MultipartFile file) {
        logger.info("In updateAccountFromXml method");
        Account accountXml = new Account();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Account.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            File fileToUpdate = new File(file.getOriginalFilename());
            file.transferTo(fileToUpdate);
            accountXml = (Account) unmarshaller.unmarshal(fileToUpdate);
            accountXml.setId(id);
            accountXml.setPhoto(accountService.getPhoto(accountXml.getId()));
            accountXml.setRole(accountService.getById(accountXml.getId()).getRole());
        } catch (JAXBException e) {
            logger.error("JAXBException: ", e);
        } catch (IOException e) {
            logger.error("Error with transfer to file. Exception: ", e);
        }
        ModelAndView modelAndView = new ModelAndView("updateUser");
        List<Phone> phones = accountXml.getPhones();
        List<Phone> homePhones = new ArrayList<>();
        List<Phone> workPhones = new ArrayList<>();
        WebUtil.getPhones(phones, homePhones, workPhones);
        modelAndView.addObject("account", accountXml);
        modelAndView.addObject("homePhones", homePhones);
        modelAndView.addObject("workPhones", workPhones);
        return modelAndView;
    }

    @PostMapping("/updatePassword")
    public String updatePassword(@Valid @ModelAttribute("changePasswordDto") ChangePasswordDto changePasswordDto,
                                 HttpServletRequest request, @SessionAttribute("id") Long idSession, Model model) {
        logger.info("In updatePassword method, id = " + idSession);
        Account account = accountService.getById(idSession);
        String oldPassword = account.getPassword();
        if (!passwordEncoder.matches(changePasswordDto.getOldPassword(), oldPassword)) {
            logger.warn("Passwords don't match, id = " + idSession);
            model.addAttribute("passwordMessage", "error");
        } else {
            account.setPassword(passwordEncoder.encode(changePasswordDto.getPassword()));
            accountService.updateAccount(account);
            request.getSession().setAttribute("user", account);
            model.addAttribute("passwordMessage", "success");
        }
        return "changePassword";
    }
}
