package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.common.Group;
import com.getjavajob.common.dto.AccountSearchDto;
import com.getjavajob.service.AccountService;
import com.getjavajob.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private static final int NUMBER_OF_RECORDS_PER_PAGE = 3;

    private final AccountService accountService;
    private final GroupService groupService;

    @Autowired
    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @GetMapping("/search")
    public ModelAndView search(@RequestParam("search") String search) {
        logger.info("In search method, search = " + search);
        int countAccount = accountService.countSearchResultAccount(search);
        int countGroup = groupService.countSearchResultGroup(search);
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("search", search);
        modelAndView.addObject("countAccount", countAccount);
        modelAndView.addObject("countGroup", countGroup);
        return modelAndView;
    }

    @RequestMapping(value = "/accountPerPage")
    public @ResponseBody
    List<AccountSearchDto> accountPerPage(@RequestParam("filter") String filter, @RequestParam("page") int page) {
        logger.info("In accountPerPage method");
        List<Account> searchAccounts = accountService.getPageAccounts(filter, getSelectedPage(page), NUMBER_OF_RECORDS_PER_PAGE);
        List<AccountSearchDto> accountSearchDto = new ArrayList<>();
        for (Account searchResult : searchAccounts) {
            AccountSearchDto dto = new AccountSearchDto();
            dto.setId(searchResult.getId());
            dto.setName(searchResult.getName());
            dto.setSurname(searchResult.getSurname());
            dto.setPhoto(searchResult.getPhoto());
            accountSearchDto.add(dto);
        }
        return accountSearchDto;
    }

    @RequestMapping(value = "/groupPerPage")
    public @ResponseBody
    List<Group> groupPerPage(@RequestParam("filter") String filter, @RequestParam("page") int page) {
        logger.info("In groupPerPage method");
        return groupService.getPageGroup(filter, getSelectedPage(page), NUMBER_OF_RECORDS_PER_PAGE);
    }

    private int getSelectedPage(int page) {
        return page == 1 ? 0 : page * NUMBER_OF_RECORDS_PER_PAGE - NUMBER_OF_RECORDS_PER_PAGE;
    }

    @RequestMapping(value = "/filterSearch")
    @ResponseBody
    public List<String> commonSearchFilter(@RequestParam("filter") String filter) {
        List<Account> filterAccounts = accountService.searchAccounts(filter);
        List<Group> filterGroup = groupService.searchGroup(filter);

        List<String> result = new ArrayList<>();
        for (Account account : filterAccounts) {
            result.add(account.getName() + " " + account.getSurname());
        }
        for (Group group : filterGroup) {
            result.add(group.getName());
        }

        Collections.sort(result);
        return result;
    }
}
