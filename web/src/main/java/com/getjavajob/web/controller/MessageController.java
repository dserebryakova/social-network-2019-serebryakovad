package com.getjavajob.web.controller;

import com.getjavajob.common.Account;
import com.getjavajob.common.Message;
import com.getjavajob.common.dto.MessageChatDto;
import com.getjavajob.common.enums.MessageType;
import com.getjavajob.service.AccountService;
import com.getjavajob.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.jms.Queue;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    private final AccountService accountService;
    private final MessageService messageService;
    private final Queue queue;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public MessageController(AccountService accountService, MessageService messageService, Queue queue, JmsTemplate jmsTemplate) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.queue = queue;
        this.jmsTemplate = jmsTemplate;
    }


    @GetMapping("/lastmess")
    public ModelAndView showLastMessage(@SessionAttribute("id") Long idSession) {
        logger.info("In showLastMessage method, id = " + idSession);
        List<Message> lastMessages = messageService.getLastMessages(idSession);
        Map<Long, Account> allMessageAccount;
        allMessageAccount = lastMessages.stream().collect(Collectors.toMap(Message::getRecipient, mes ->
                accountService.getById(mes.getRecipient()), (a, b) -> b));
        ModelAndView modelAndView = new ModelAndView("messages");
        modelAndView.addObject("lastMessages", lastMessages);
        modelAndView.addObject("allMessageAccount", allMessageAccount);
        return modelAndView;
    }

    @GetMapping("/chat")
    public ModelAndView showChatPage(@RequestParam("idSender") Long idSender,
                                     @RequestParam("idRecipient") Long idRecipient,
                                     @SessionAttribute("id") Long idSession) {
        logger.info("In showChatPage method, id = " + idSession);
        Account sessionAccount = accountService.getById(idSession);
        long idInterlocutor;
        if (idSender.equals(sessionAccount.getId())) {
            idInterlocutor = idRecipient;
        } else {
            idInterlocutor = idSender;
        }
        List<Message> messages = messageService.getMessages(idSession, idInterlocutor);
        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("idInterlocutor", idInterlocutor);
        return modelAndView;
    }

    @PostMapping("/addMessage")
    public String addMessage(@RequestParam("idRecipient") Long idRecipient,
                             @RequestParam("messanger") String messager,
                             @RequestParam("typeMess") String type,
                             @RequestParam(required = false, name = "file_uploaded") MultipartFile photo,
                             @SessionAttribute("id") long idSession) throws IOException {
        logger.info("In addMessage method, id = " + idSession);
        Account sender = accountService.getById(idSession);
        Message message = new Message();
        message.setSender(sender);
        message.setRecipient(idRecipient);
        String redirect = null;
        message.setText(messager);
        if (!photo.isEmpty()) {
            message.setPicture(photo.getBytes());
        }
        switch (type) {
            case "wallAccount":
                message.setMessageType(MessageType.ACCOUNT_WALL);
                redirect = "redirect:/home?id=" + idRecipient;
                break;
            case "wallGroup":
                message.setMessageType(MessageType.GROUP_WALL);
                redirect = "redirect:/homeGroup?id=" + idRecipient;
                break;
        }
        messageService.addMessage(message);
        return redirect;
    }

    @PostMapping("/deleteMessage")
    public String deleteMessage(@RequestParam("idRow") Long idRow, @RequestParam("id") Long id, @RequestParam("typeMess") String type) {
        logger.info("In deleteMessage method, id = " + id);
        messageService.delete(idRow);
        String redirect = "/";

        if (type.equals("wallAccount")) {
            redirect = "redirect:/home?id=" + id;
        } else if (type.equals("wallGroup")) {
            redirect = "redirect:/homeGroup?id=" + id;
        }
        return redirect;
    }

    @MessageMapping("/sendMessage/{to}")
    @SendTo("/topic/{to}")
    public MessageChatDto sendPersonalMessage(MessageChatDto messageChatDto) {
        Account accountSender = accountService.getById(messageChatDto.getSender());
        messageChatDto.setSendTime(messageService.getDate());
        messageChatDto.setNameSender(accountSender.getName());
        messageChatDto.setSurnameSender(accountSender.getSurname());
        messageService.addChatMessage(messageChatDto, accountSender);
        jmsTemplate.convertAndSend(queue, messageChatDto.getJmsMessage());
        return messageChatDto;
    }
}
