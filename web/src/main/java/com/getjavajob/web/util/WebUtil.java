package com.getjavajob.web.util;

import com.getjavajob.common.Account;
import com.getjavajob.common.Phone;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.common.enums.PhoneType.HOME;

public class WebUtil {
    public static void getPhones(List<Phone> phones, List<Phone> homePhones, List<Phone> workPhones) {
        if (!phones.isEmpty()) {
            for (Phone phone : phones) {
                if (phone.getPhoneType() == HOME) {
                    homePhones.add(phone);
                } else {
                    workPhones.add(phone);
                }
            }
        }
    }

    public static List<Phone> constructPhones(Account account) {
        List<Phone> phones = account.getPhones();
        List<Phone> result = new ArrayList<>();
        for (Phone phone : phones) {
            if (phone.getNumber() != null) {
                phone.setPhoneOwner(account);
                result.add(phone);
            }
        }
        return result;
    }
}
