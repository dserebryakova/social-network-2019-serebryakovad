package com.getjavajob.web.security;

import com.getjavajob.common.Account;
import com.getjavajob.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class SocNetAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final AccountService accountService;

    @Autowired
    public SocNetAuthenticationSuccessHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
        HttpSession session = httpServletRequest.getSession();
        Account account = accountService.getAccountByEmail(authentication.getName());
        session.setAttribute("user", account);
        session.setAttribute("id", account.getId());
        session.setAttribute("role", account.getRole());
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.sendRedirect("/home?id=" + account.getId());
    }
}
