package com.getjavajob.web.security;

import com.getjavajob.common.Account;
import com.getjavajob.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class SocNetUserDetailsServiceImpl implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(SocNetUserDetailsServiceImpl.class);

    private final AccountService accountService;

    @Autowired
    public SocNetUserDetailsServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountService.getAccountByEmail(username);
        if (account == null) {
            String errorMessage = String.format("Username with email: %s not found", username);
            logger.error(errorMessage);
            throw new UsernameNotFoundException(errorMessage);
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(account.getRole().toString()));

        return new User(account.getEmail(), account.getPassword(), true, true, true,
                true, authorities);
    }
}
