package com.getjavajob.web.config;

import com.getjavajob.web.security.SocNetAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final SocNetAuthenticationSuccessHandler socNetAuthenticationSuccessHandler;

    private static final String[] ALLOWED_PATH = {
            "/",
            "/login",
            "/loginForm",
            "/register",
            "/checkRegister",
            "/resources/**"
    };

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService, SocNetAuthenticationSuccessHandler socNetAuthenticationSuccessHandler) {
        this.userDetailsService = userDetailsService;
        this.socNetAuthenticationSuccessHandler = socNetAuthenticationSuccessHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(ALLOWED_PATH).permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().successHandler(socNetAuthenticationSuccessHandler)
                .loginPage("/loginForm")
                .loginProcessingUrl("/login").failureUrl("/loginForm?error")
                .and()
                .rememberMe()
                .key("Socnet_key")
                .rememberMeCookieName("remember-me-Socnet")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .and()
                .exceptionHandling().accessDeniedPage("/accessError")
                .and()
                .csrf().disable();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
