package com.getjavajob.common.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageJmsDto {
    private Long sender;
    private Long recipient;
    private String text;
    private String sendTime;
}
