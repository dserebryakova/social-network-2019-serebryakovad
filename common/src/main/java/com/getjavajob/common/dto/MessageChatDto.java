package com.getjavajob.common.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageChatDto {
    private Long sender;
    private String nameSender;
    private String surnameSender;
    private Long recipient;
    private String text;
    private String sendTime;

    public MessageJmsDto getJmsMessage(){
        return new MessageJmsDto(sender, recipient, text, sendTime);
    }
}
