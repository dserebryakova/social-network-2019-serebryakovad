package com.getjavajob.common.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccountSearchDto {
    private Long id;
    private String name;
    private String surname;
    private byte[] photo;
}
