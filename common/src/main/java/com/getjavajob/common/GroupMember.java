package com.getjavajob.common;

import com.getjavajob.common.enums.GroupRole;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "group_members")
public class GroupMember extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_group")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "id_member")
    private Account member;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private GroupRole groupRole;
}
