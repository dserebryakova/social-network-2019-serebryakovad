package com.getjavajob.common;

import com.getjavajob.common.enums.PhoneType;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "phones")
@XmlAccessorType(XmlAccessType.FIELD)
public class Phone extends BaseEntity {

    @Column(nullable = false)
    private String number;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private PhoneType phoneType;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "id_owner_phone")
    @XmlTransient
    private Account phoneOwner;
}
