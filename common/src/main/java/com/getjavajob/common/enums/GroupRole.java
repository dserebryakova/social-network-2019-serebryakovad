package com.getjavajob.common.enums;

public enum GroupRole {

    SUBSCRIBER, MEMBER, MODER, ADMIN

}
