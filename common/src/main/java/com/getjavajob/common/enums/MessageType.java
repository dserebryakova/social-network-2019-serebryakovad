package com.getjavajob.common.enums;

public enum MessageType {

    PRIVATE, ACCOUNT_WALL, GROUP_WALL

}
