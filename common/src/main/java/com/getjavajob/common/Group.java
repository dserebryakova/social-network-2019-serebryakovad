package com.getjavajob.common;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "groups_")
public class Group extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(name = "reg_date", nullable = false)
    private String regDate;

    @Column(name = "id_owner")
    private Long idOwner;

    @Column
    private String thematics;

    @Column
    private String info;

    @Column
    private byte[] photo;
}
