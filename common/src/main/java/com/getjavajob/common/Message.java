package com.getjavajob.common;

import com.getjavajob.common.enums.MessageType;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "messages")
public class Message extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_sender")
    private Account sender;

    @Column(name = "id_recipient")
    private Long recipient;

    @Column
    private String text;

    @Column
    private byte[] picture;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private MessageType messageType;

    @Column(name = "send_time")
    private String sendTime;
}
