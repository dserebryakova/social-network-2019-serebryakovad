package com.getjavajob.common;

import com.getjavajob.common.enums.Role;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column
    private String surname;

    @Column
    private String patronymic;

    @Column
    private String birthdate;

    @Column(name = "address_home")
    private String addressHome;

    @Column(name = "address_work")
    private String addressWork;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    @XmlTransient
    private String password;

    @Column(name = "reg_date", nullable = false)
    @XmlTransient
    private String regDate;

    @Column
    private String icq;

    @Column
    private String skype;

    @Column(name = "mood_status")
    private String moodStatus;

    @Column
    @XmlTransient
    private byte[] photo;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @XmlTransient
    private Role role;

    @ToString.Exclude
    @OneToMany(mappedBy = "phoneOwner", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Phone> phones = new ArrayList<>();

    public Account(String name, String email, String password, String regDate, Role role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.regDate = regDate;
        this.role = role;
    }
}
