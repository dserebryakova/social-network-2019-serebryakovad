package com.getjavajob.common;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "friend")
public class Friend extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_master", nullable = false)
    private Account accountMaster;

    @ManyToOne
    @JoinColumn(name = "id_friend", nullable = false)
    private Account accountFriend;

    @Column
    private boolean accepted;

    public Friend(Account accountMaster, Account accountFriend) {
        this.accountMaster = accountMaster;
        this.accountFriend = accountFriend;
    }
}
